/**********************************************************************
 This file is part of the OpenEAI sample, reference implementation,
 and deployment management suite created by Tod Jackson
 (tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
 the University of Illinois Urbana-Champaign.

 Copyright (C) 2002 The OpenEAI Software Foundation

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 For specific licensing details and examples of how this software
 can be used to implement integrations for your enterprise, visit
 http://www.OpenEai.org/licensing.
 */

package org.openeai.implementations.applications.testsuite;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;
import org.jdom.transform.JDOMResult;
import org.jdom.transform.JDOMSource;
import org.openeai.OpenEaiObject;
import org.openeai.afa.ScheduledCommandException;
import org.openeai.config.AppConfig;
import org.openeai.config.CommandConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.config.PropertyConfig;
import org.openeai.jms.producer.PointToPointProducer;
import org.openeai.jms.producer.PubSubProducer;
import org.openeai.layouts.EnterpriseLayoutException;
import org.openeai.moa.ActionableEnterpriseObject;
import org.openeai.moa.XmlEnterpriseObject;
import org.openeai.moa.XmlEnterpriseObjectException;
import org.openeai.moa.objects.resources.Result;
import org.openeai.moa.objects.testsuite.TestId;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * This class implements the default behaviour for a test step.
 */
public class DefaultTestStepExecutor extends OpenEaiObject implements TestStepExecutor {
    private static Logger LOG = Logger.getLogger(DefaultTestStepExecutor.class);
    private static String LOGTAG = "[" + DefaultTestStepExecutor.class.getSimpleName() + "]";
    /**
     * xsl path for xslt transformation
     */
    public static final String XSL_URI = "xslUri";

    private int m_producerTimeoutInterval = 30000; // default

    private AppConfig m_appConfig = null;

    // --------------------- public Methods ---------------------

    public DefaultTestStepExecutor() {

    }

    /**
     * This method will take a test step and execute it.
     * 
     * @param config
     *            for the application config
     * @param commandConfig
     *            for the scheduled test command
     * @param producerName
     *            of the producer to use
     * @param testId
     *            id for this test case/step
     * @param eTestStep
     *            element from the test document
     * @return a StepExecutionInfo object filled with the results of the test.
     * @throws ScheduledCommandException
     *             if any un-expected errors occur when executing the test step.
     */
    @Override
    public StepExecutionInfo executeTestStep(AppConfig config, CommandConfig commandConfig, String producerName, TestId testId,
            Element eTestStep) throws ScheduledCommandException {

        m_appConfig = config;

        try {
            PropertyConfig pConfig = (PropertyConfig) getAppConfig().getObject("TestSuiteProperties");

            m_producerTimeoutInterval = Integer.parseInt(pConfig.getProperties().getProperty("producerTimeoutInterval", "30000"));
        } catch (EnterpriseConfigurationObjectException exp) {
            logger.fatal(exp.getMessage(), exp);
            throw new ScheduledCommandException(exp.getMessage(), exp);
        }

        String stepNumber = eTestStep.getAttribute("number").getValue();
        try {
            testId.setTestStepNumber(stepNumber);
        } catch (EnterpriseFieldException e) {
            String errMessage = "Exception setting TestStepNumber on TestId object.  Cannot execute TestStep '" + stepNumber
                    + "' for TestSuite " + testId.getTestSuiteName() + "  TestSeries " + testId.getTestSeriesNumber() + " TestCase "
                    + testId.getTestCaseNumber() + "  Exception: " + e.getMessage();
            throw new ScheduledCommandException(errMessage, e);
        }

        String stepId = testId.getTestSuiteName() + "-" + testId.getTestSeriesNumber() + "-" + testId.getTestCaseNumber() + "-"
                + testId.getTestStepNumber();

        logger.info("Executing TestStep " + stepId);

        Element eGenRequest = eTestStep.getChild(TestSuite.GENERATE_REQUEST);
        if (eGenRequest != null) {
            return executeGenerateRequest(producerName, testId, eGenRequest);
        }

        Element eCreateRequest = eTestStep.getChild(TestSuite.CREATE_REQUEST);
        if (eCreateRequest != null) {
            return executeCreateRequest(producerName, testId, eCreateRequest);
        }

        Element eCreateSync = eTestStep.getChild(TestSuite.CREATE_SYNC);
        if (eCreateSync != null) {
            return executeCreateSync(producerName, testId, eCreateSync);
        }

        Element eDeleteRequest = eTestStep.getChild(TestSuite.DELETE_REQUEST);
        if (eDeleteRequest != null) {
            return executeDeleteRequest(producerName, testId, eDeleteRequest);
        }

        Element eDeleteSync = eTestStep.getChild(TestSuite.DELETE_SYNC);
        if (eDeleteSync != null) {
            return executeDeleteSync(producerName, testId, eDeleteSync);
        }

        Element eUpdateRequest = eTestStep.getChild(TestSuite.UPDATE_REQUEST);
        if (eUpdateRequest != null) {
            return executeUpdateRequest(producerName, testId, eUpdateRequest);
        }

        Element eUpdateSync = eTestStep.getChild(TestSuite.UPDATE_SYNC);
        if (eUpdateSync != null) {
            return executeUpdateSync(producerName, testId, eUpdateSync);
        }

        Element eQueryRequest = eTestStep.getChild(TestSuite.QUERY_REQUEST);
        if (eQueryRequest != null) {
            return executeQueryRequest(producerName, testId, eQueryRequest);
        }

        Element eTearDownRequest = eTestStep.getChild("TearDownRequest");
        if (eTearDownRequest != null) {
            return executeTearDownRequest(producerName, testId, eTearDownRequest);
        }

        return null;
    }

    /**
     * This method will verify a sync was processed properly by the consuming
     * system by potentially sending a QueryRequest to that system for some data
     * that obtains at least part of it's data from the object included in the
     * sync message.
     * <P>
     * 
     * @param testId
     *            the TestId object that contains the name of the step.
     * @param eSync
     *            the CreateSync element obtained from the TestSuite. This may
     *            include an optional QueryRequest element that can be used to
     *            verify the sync was processed properly by the consuming
     *            gateway.
     * @return Sting a message that may contain information if the QueryRequest
     *         failed as expected. A null will be returned if the test passed
     *         and there were no expected errors.
     * @throws ScheduledCommandException
     *             if any un-expected errors occur when validating the sync with
     *             the QueryRequest. This will include any errors returned from
     *             the gateway that processed the QueryRequest.
     **/
    protected StepExecutionInfo validateSyncByQuery(TestId testId, Element eSync) throws ScheduledCommandException {
        StepExecutionInfo sei = new StepExecutionInfo();
        Element eQueryRequest = eSync.getChild("QueryRequest");
        if (eQueryRequest != null) {
            // First, see if there's a sleepInterval that should be used to
            // pause
            // between publishing the sync and performing the query.
            Attribute aSleep = eSync.getAttribute("sleepInterval");
            if (aSleep != null) {
                try {
                    long sleep = Long.parseLong(aSleep.getValue());
                    logger.info("Sleeping '" + sleep + "' milliseconds before performing query to verify Sync.");
                    Thread.sleep(sleep);
                } catch (Exception e) {
                    logger.fatal("Exception occurred converting the CreateSync 'sleepInterval' " + aSleep.getValue()
                            + " to a number.  Can't sleep between sync and Query.  Processing will continue.  Exception is: "
                            + e.getMessage());
                }
            }
            // Now, execute the QueryRequest to verify the sync was processed
            // properly.
            Attribute aProducer = eQueryRequest.getAttribute("producerName");
            if (aProducer == null) {
                // error
                String errMessage = "Could not find a 'producerName' Attribute in the QueryRequest element "
                        + "being used to validate a Sync message for TestId '" + testId + "'";
                throw new ScheduledCommandException(errMessage);
            }
            String producerName = aProducer.getValue();
            return executeQueryRequest(producerName, testId, eQueryRequest);
        } else {
            return sei;
        }
    }

    /**
     * Send a Generate request and validate the reply returned. Any resulting
     * Sync messages will be verified by the SyncVerificationCommand later.
     * <P>
     * 
     * @param producerName
     *            a String indicating the name of the producer to use to send
     *            the request. This can be associated to an entire TestSeries or
     *            to an individual TestStep.
     * @param testId
     *            the TestId object that contains the name of the step.
     * @param eRequest
     *            the CreateRequest element obtained from the TestSuite.
     * @return Sting a message that may contain information if the request
     *         failed as expected. A null will be returned if the test passed
     *         and there were no expected errors.
     * @throws ScheduledCommandException
     *             if any un-expected errors occur when processing the request.
     *             This will include any errors returned from the gateway that
     *             processed the request.
     **/
    protected StepExecutionInfo executeGenerateRequest(String producerName, TestId testId, Element eRequest)
            throws ScheduledCommandException {
        /*
         * <!ELEMENT GenerateRequest (GenerateData, TestResult, ResponseData,
         * ExpectedCreateSync*)> <!ATTLIST GenerateRequest messageCategory CDATA
         * #REQUIRED generateObjectName CDATA #REQUIRED messageObjectName CDATA
         * #REQUIRED messageRelease CDATA #REQUIRED messagePriority (0 | 1 | 2 |
         * 3 | 4 | 5 | 6 | 7 | 8 | 9) #REQUIRED > <!ELEMENT GenerateData
         * (UnknownPerson)> <!ELEMENT ResponseData (InstitutionalIdentity)>
         */

        StepExecutionInfo sei = new StepExecutionInfo();
        String stepId = testId.getTestSuiteName() + "-" + testId.getTestSeriesNumber() + "-" + testId.getTestCaseNumber() + "-"
                + testId.getTestStepNumber();

        String messageObjectName = eRequest.getAttribute("messageObjectName").getValue();
        String generateObjectName = eRequest.getAttribute("generateObjectName").getValue();

        // get the newData from the CreateRequest object passed to this method
        Element eGenData = eRequest.getChild("GenerateData");
        Element eTestResult = eRequest.getChild("TestResult");
        Element eResponseData = eRequest.getChild("ResponseData");
        String expectedStatus = eTestResult.getAttribute("status").getValue();

        // now get whatever message object is there
        List lGenDataChildren = eGenData.getChildren(); // should only ever be
                                                        // one!
        if (lGenDataChildren.size() != 1) {
            String errMessage = "Could not find a GenerateData element in the GenerateRequest object passed in.";
            logger.fatal(errMessage);
            throw new ScheduledCommandException(errMessage);
        }
        Element eGenDataObject = (Element) lGenDataChildren.get(0);
        if (eGenDataObject == null) {
            String errMessage = "Could not find a data element in the GenerateData element in the GenerateRequest object passed in.";
            logger.fatal(errMessage);
            throw new ScheduledCommandException(errMessage);
        }

        // now get whatever message object is there
        List lResponseDataChildren = eResponseData.getChildren(); // should only
                                                                  // ever be
                                                                  // one!
        if (lResponseDataChildren.size() != 1) {
            String errMessage = "Could not find a ResponseData element in the GenerateRequest object passed in.";
            logger.fatal(errMessage);
            throw new ScheduledCommandException(errMessage);
        }
        Element eResponseDataObject = (Element) lResponseDataChildren.get(0);
        if (eResponseDataObject == null) {
            String errMessage = "Could not find a data element in the ResponseData element in the GenerateRequest object passed in.";
            logger.fatal(errMessage);
            throw new ScheduledCommandException(errMessage);
        }

        // retrieve the message object associated to this seriesNumber from
        // AppConfig
        ActionableEnterpriseObject newJeo;
        XmlEnterpriseObject expectedGeneratedXeo;
        XmlEnterpriseObject genXeo;
        try {
            newJeo = (ActionableEnterpriseObject) getObjectFromAppConfig(messageObjectName,
                    eRequest.getAttribute("messageRelease").getValue());
            expectedGeneratedXeo = getObjectFromAppConfig(messageObjectName, eRequest.getAttribute("messageRelease").getValue());
            genXeo = getObjectFromAppConfig(generateObjectName, eRequest.getAttribute("messageRelease").getValue());
            try {
                // build the message object used for the generate from the
                // GenerateRequest/GenerateData element
                genXeo.buildObjectFromInput(eGenDataObject);
            } catch (Exception e) {
                String errMessage = "Exception building XML Message Object " + newJeo.getClass().getName()
                        + " from 'GenerateRequest/GenerateData' " + generateObjectName + " Element.  " + e.getMessage();
                logger.fatal(errMessage, e);
                throw new ScheduledCommandException(errMessage, e);
            }
        } catch (Exception e) {
            String errMessage = "Could not find a Message Object named " + messageObjectName
                    + " in the AppConfig object associated to the TestSuite Application.  Exception: " + e.getMessage();
            throw new ScheduledCommandException(errMessage, e);
        }

        // retrieve the producer associated to this stepId from AppConfig
        PointToPointProducer p2p = getPointToPointProducer(producerName);

        // call the generate method on the message object passing the producer
        try {
            newJeo.getXmlEnterpriseObject().setTestId(testId);

            // start timer
            long startTime = System.currentTimeMillis();

            // perform the generate request
            List v = newJeo.generate(genXeo, p2p);

            // stop timer and store elapsed time for averages
            sei.setElapsedTime(System.currentTimeMillis() - startTime);
            sei.setCategory(TestSuiteScheduledCommand.GENERATE_ACTION);

            // verify contents of generated object (should match ResponseData
            // element)
            if (v.size() != 1) {
                String errMessage = "Incorrect number of message objects returned in response.  Got " + v.size() + " expected 1";
                throw new ScheduledCommandException(errMessage);
            }

            boolean foundReturnedXeo = false;
            XmlEnterpriseObject returnedXeo = null;
            for (int i = 0; i < v.size(); i++) {
                returnedXeo = (XmlEnterpriseObject) v.get(i);

                // TJ 4/20/2012 - Add data from returnedXeo to sei.responseMap
                // so it can be used later
                addObjectToResponseMap(stepId, returnedXeo);

                // TJ - 12/6/2011
                returnedXeo = applyTransformation(eResponseData.getAttributeValue(XSL_URI), returnedXeo);
                // END TJ - 12/6/2011

                // TJ 4/20/2012: apply any data that's been saved for this
                // step/object
                // from the shared TestSuiteScheduledCommand.responseMap to
                // the element that will be used to populate the returnedXeo
                applyDataToElementFromResponseMap(eResponseDataObject);

                expectedGeneratedXeo.buildObjectFromInput(eResponseDataObject);

                // check to see if the returnedXeo matches the expected
                // generated object as defined in the test suite
                if (expectedGeneratedXeo.equals(returnedXeo)) {
                    foundReturnedXeo = true;
                    break;
                }
            }
            if (!foundReturnedXeo) {
                String errMessage = " Mismatch: " + "\n\nExpected=" + expectedGeneratedXeo.toXmlString() + "\n\nReturned="
                        + returnedXeo.toXmlString() + "\n\ndiff="
                        + StringUtils.difference(expectedGeneratedXeo.toXmlString(), returnedXeo.toXmlString());
                throw new ScheduledCommandException(errMessage);
            }
        } catch (Exception e) {
            // if we catch an exception, we have to check to see if the test was
            // suppose to fail,
            // if it was, this step will have passed. If not, this step has
            // failed!
            String errorFromGateway = e.getMessage();
            if (expectedStatus.equalsIgnoreCase("success")) {
                logger.fatal("The GenerateRequest failed and it was not expected!", e);
                throw new ScheduledCommandException(e);
            } else {
                sei.setExecutionMessage("The GenerateRequest failed but this is expected.  Reason: " + errorFromGateway);
            }
        }
        return sei;
    }

    /**
     * This method applies a transformation to the data returned in a
     * Query.Provide-Reply or Generate.Response-Reply using the 'xslUri'
     * attribute associated to the ProvideData and/or ResponseData elements. If
     * present, this attribute should point to a valid XSLT file (in URI form)
     * that will 'scrub' the returned data into a form that can be compared to
     * hard-coded expected results in the test suite. This allows us to
     * successfully test things like a Generate-Request/Reply which often
     * contains data that can't be predicted.
     * 
     * @param xslUri
     *            xsl file path as specified in the test suite
     * @param returnedXeo
     *            the XmlEnterpriseObject that was returned in a
     *            Generate.Response-Reply or Query.Provide-Reply.
     * @return a new Xeo containing the transformed content
     * @throws ScheduledCommandException
     *             if the transformation fails
     * @throws EnterpriseLayoutException
     *             on error
     * @author Tod Jackson
     * @since 12/6/2011 @
     */
    protected XmlEnterpriseObject applyTransformation(String xslUri, XmlEnterpriseObject returnedXeo)
            throws ScheduledCommandException, EnterpriseLayoutException {

        LOG.info("xslUri=" + xslUri);
        if (xslUri == null)
            return returnedXeo; // no transformation

        // apply transformation to the data returned (returnedXeo as an XML
        // element):
        // - convert returnedXeo to an element
        Element eReturnedXeo = (Element) returnedXeo.buildOutputFromObject();
        LOG.info("beforeTransform=" + new XMLOutputter().outputString(eReturnedXeo));

        // - apply transformation to the returned XEO element
        // JDOMResult out = new JDOMResult();
        // try {
        // // initialize transformer using xslUri
        // Transformer transformer =
        // TransformerFactory.newInstance().newTransformer(new
        // StreamSource(xslUri));
        // Document sourceDoc = new Document(eReturnedXeo);
        // transformer.transform(new JDOMSource(sourceDoc), out);
        // } catch (TransformerException e) {
        // String errMsg = "An error occurred transforming the " +
        // eReturnedXeo.getName() + " element in the ResponseData received.";
        // LOG.fatal(errMsg, e);
        // throw new ScheduledCommandException(errMsg, e);
        // }
        //
        // // Build the output document.
        // Document outDoc = out.getDocument();
        // Element eTransformedReturnedXeo = outDoc.getRootElement();
        // eTransformedReturnedXeo.detach();

        Element eTransformedReturnedXeo = applyTransformation(xslUri, eReturnedXeo);
        LOG.info("afterTransform=" + new XMLOutputter().outputString(eTransformedReturnedXeo));

        // - get a new Xeo from app config
        // a new Xeo is used instead of reusing returnedXeo due to a bug in
        // buildObjectFromInput
        // where repeatable elements (as defined by the DTD) were duplicated
        // causing the built
        // object to not match the input
        XmlEnterpriseObject newXeo;
        try {
            newXeo = (XmlEnterpriseObject) getAppConfig().getObjectByType(returnedXeo.getClass().getName());
        } catch (EnterpriseConfigurationObjectException e) {
            String errMsg = "An error occurred getting a new EO for " + returnedXeo.getClass().getName();
            LOG.fatal(errMsg, e);
            throw new ScheduledCommandException(errMsg, e);
        }

        // - build a new Xeo from transformed element
        newXeo.buildObjectFromInput(eTransformedReturnedXeo);

        // at this point, the newXeo should be in a state where it
        // can be compared to the expected results which may have hard-coded
        // data like "XXXXX" or something which should allow us to compare
        // the returned object to the expected results in the test suite.
        try {
            LOG.info("newXeo=" + newXeo.toXmlString());
        } catch (XmlEnterpriseObjectException e) {
            LOG.info("Ignoring error while logging debug info", e);
        }

        return newXeo;
    }

    protected static Element applyTransformation(String xslUri, Element elementToBeTransformed) throws ScheduledCommandException {
        LOG.info("xslUri=" + xslUri);
        if (xslUri == null)
            return elementToBeTransformed;
        JDOMResult out = new JDOMResult();
        elementToBeTransformed.detach();
        try {
            // initialize transformer using xslUri
            Transformer transformer = TransformerFactory.newInstance().newTransformer(new StreamSource(xslUri));
            Document sourceDoc = new Document(elementToBeTransformed);
            transformer.transform(new JDOMSource(sourceDoc), out);
        } catch (TransformerException te) {
            // An error occurred performing the transformation.
            LOG.fatal(te.getMessage(), te);
            String errMsg = "An error occurred transforming the " + elementToBeTransformed.getName()
                    + " element in the ResponseData received.  The exception is: " + te.getMessage();
            LOG.error(errMsg);
            throw new ScheduledCommandException(errMsg);
        }
        Document outDoc = out.getDocument();
        Element elementTransformed = outDoc.getRootElement();
        elementTransformed.detach();
        LOG.info("elementToBeTransformed=" + new XMLOutputter().outputString(elementToBeTransformed));
        LOG.info("elementTransformed=" + new XMLOutputter().outputString(elementTransformed));
        return elementTransformed;
    }

    /**
     * Send a Create request and validate the reply returned. Any resulting Sync
     * messages will be verified by the SyncVerificationCommand later.
     * <P>
     * 
     * @param producerName
     *            a String indicating the name of the producer to use to send
     *            the request. This can be associated to an entire TestSeries or
     *            to an individual TestStep.
     * @param testId
     *            the TestId object that contains the name of the step.
     * @param eRequest
     *            the CreateRequest element obtained from the TestSuite.
     * @return Sting a message that may contain information if the request
     *         failed as expected. A null will be returned if the test passed
     *         and there were no expected errors.
     * @throws ScheduledCommandException
     *             if any un-expected errors occur when processing the request.
     *             This will include any errors returned from the gateway that
     *             processed the request.
     **/
    protected StepExecutionInfo executeCreateRequest(String producerName, TestId testId, Element eRequest)
            throws ScheduledCommandException {

        StepExecutionInfo sei = new StepExecutionInfo();

        String messageObjectName = eRequest.getAttribute("messageObjectName").getValue();

        // get the newData from the CreateRequest object passed to this method
        Element eNewData = eRequest.getChild("NewData");
        Element eTestResult = eRequest.getChild("TestResult");
        String expectedStatus = eTestResult.getAttribute("status").getValue();

        // now get whatever message object is there
        List lNewDataChildren = eNewData.getChildren(); // should only ever be
                                                        // one!
        if (lNewDataChildren.size() != 1) {
            // throw exception
        }
        Element eCreateRequestObject = (Element) lNewDataChildren.get(0);
        if (eCreateRequestObject == null) {
            // throw exception!
            logger.fatal("Could not find a NewData object in the CreateRequest object passed in.");
        }

        // retrieve the message object associated to this seriesNumber from
        // AppConfig
        ActionableEnterpriseObject newJeo;
        try {
            newJeo = (ActionableEnterpriseObject) getObjectFromAppConfig(messageObjectName,
                    eRequest.getAttribute("messageRelease").getValue());
        } catch (Exception e) {
            // throw exception!
            String errMessage = "Could not find a Message Object named " + messageObjectName
                    + " in the AppConfig object associated to the TestSuite Application.  Exception: " + e.getMessage();
            throw new ScheduledCommandException(errMessage, e);
        }

        try {
            // build the message object from the createRequest.newData passed in
            newJeo.getXmlEnterpriseObject().buildObjectFromInput(eCreateRequestObject);
        } catch (Exception e) {
            String errMessage = "Exception building JMS Message Object " + newJeo.getClass().getName()
                    + " from 'CreateRequest/NewData' Element.  " + e.getMessage();
            logger.fatal(errMessage, e);
            throw new ScheduledCommandException(errMessage, e);
        }

        // retrieve the producer associated to this stepId from AppConfig
        PointToPointProducer p2p = getPointToPointProducer(producerName);

        // call the create method on the message object passing the producer
        try {
            newJeo.getXmlEnterpriseObject().setTestId(testId);

            // start timer
            long startTime = System.currentTimeMillis();

            // perform the create request
            Result result = (Result) newJeo.create(p2p);

            // stop timer and store elapsed time for averages
            sei.setElapsedTime(System.currentTimeMillis() - startTime);
            sei.setCategory(TestSuiteScheduledCommand.CREATE_ACTION);

            try {
                checkResult(result, eTestResult);
            } catch (Exception exResult) {
                logger.fatal(exResult.getMessage(), exResult);
                throw new ScheduledCommandException(exResult.getMessage(), exResult);
            }
        } catch (Exception e) {
            // if we catch an exception, we have to check to see if the test was
            // suppose to fail,
            // if it was, this step will have passed. If not, this step has
            // failed!
            String errorFromGateway = e.getMessage();
            if (expectedStatus.equalsIgnoreCase("success")) {
                logger.fatal("The CreateRequest failed and it was not expected!", e);
                throw new ScheduledCommandException(e);
            } else {
                sei.setExecutionMessage("The CreateRequest failed but this is expected.  Reason: " + errorFromGateway);
            }
        }
        return sei;
    }

    /**
     * Publish a Create sync message and optionally verify the sync was
     * processed properly by executing a QueryRequest.
     * <P>
     * 
     * @param producerName
     *            a String indicating the name of the producer to use to publish
     *            the sync. This can be associated to an entire TestSeries or to
     *            an individual TestStep.
     * @param testId
     *            the TestId object that contains the name of the step.
     * @param eSync
     *            the CreateSync element obtained from the TestSuite. This may
     *            include an optional QueryRequest element that can be used to
     *            verify the sync was processed properly by the consuming
     *            gateway.
     * @return Sting a message that may contain information if the QueryRequest
     *         failed as expected. A null will be returned if the test passed
     *         and there were no expected errors.
     * @throws ScheduledCommandException
     *             if any un-expected errors occur when validating the sync with
     *             the QueryRequest. This will include any errors returned from
     *             the gateway that processed the QueryRequest.
     **/
    protected StepExecutionInfo executeCreateSync(String producerName, TestId testId, Element eSync) throws ScheduledCommandException {

        String messageObjectName = eSync.getAttribute("messageObjectName").getValue();

        // get the newData from the CreateRequest object passed to this method
        Element eNewData = eSync.getChild("NewData");

        // now get whatever message object is there
        List lNewDataChildren = eNewData.getChildren(); // should only ever be
                                                        // one!
        if (lNewDataChildren.size() != 1) {
            // throw exception
        }
        Element eCreateSyncObject = (Element) lNewDataChildren.get(0);
        if (eCreateSyncObject == null) {
            // throw exception!
            logger.fatal("Could not find a NewData object in the CreateSync object passed in.");
        }

        // retrieve the message object associated to this seriesNumber from
        // AppConfig
        ActionableEnterpriseObject newJeo;
        try {
            newJeo = (ActionableEnterpriseObject) getObjectFromAppConfig(messageObjectName,
                    eSync.getAttribute("messageRelease").getValue());
        } catch (Exception e) {
            // throw exception!
            String errMessage = "Could not find a Message Object named " + messageObjectName
                    + " in the AppConfig object associated to the TestSuite Application.  Exception: " + e.getMessage();
            throw new ScheduledCommandException(errMessage, e);
        }
        try {
            // build the message object from the createRequest.newData passed in
            newJeo.getXmlEnterpriseObject().buildObjectFromInput(eCreateSyncObject);
        } catch (Exception e) {
            String errMessage = "Exception building JMS Message Object " + newJeo.getClass().getName()
                    + " from 'CreateSync/NewData' Element.  " + e.getMessage();
            logger.fatal(errMessage, e);
            throw new ScheduledCommandException(errMessage, e);
        }

        // retrieve the producer associated to this stepId from AppConfig

        // TODO: Make this us a producer pool?
        PubSubProducer p;
        try {
            p = (PubSubProducer) getAppConfig().getObject(producerName);
        } catch (Exception e) {
            // very bad error
            String errMessage = "Could not find a Producer named " + producerName
                    + " in the AppConfig object associated to the TestSuite Application.  Exception: " + e.getMessage();
            throw new ScheduledCommandException(errMessage, e);
        }

        // call the create method on the message object passing the producer
        try {
            newJeo.getXmlEnterpriseObject().setTestId(testId);
            newJeo.createSync(p);
        } catch (Exception e) {
            logger.fatal(e.getMessage(), e);
            throw new ScheduledCommandException(e.getMessage(), e);
        }

        // now, if there's a QueryRequest associated to the Sync, execute it to
        // verify the sync.
        return validateSyncByQuery(testId, eSync);
    }

    /**
     * Send a Delete request and validate the reply returned. Any resulting Sync
     * messages will be verified by the SyncVerificationCommand later.
     * <P>
     * 
     * @param producerName
     *            a String indicating the name of the producer to use to send
     *            the request. This can be associated to an entire TestSeries or
     *            to an individual TestStep.
     * @param testId
     *            the TestId object that contains the name of the step.
     * @param eRequest
     *            the DeleteRequest element obtained from the TestSuite.
     * @return Sting a message that may contain information if the request
     *         failed as expected. A null will be returned if the test passed
     *         and there were no expected errors.
     * @throws ScheduledCommandException
     *             if any un-expected errors occur when processing the request.
     *             This will include any errors returned from the gateway that
     *             processed the request.
     **/
    protected StepExecutionInfo executeDeleteRequest(String producerName, TestId testId, Element eRequest)
            throws ScheduledCommandException {

        StepExecutionInfo sei = new StepExecutionInfo();

        String messageObjectName = eRequest.getAttribute("messageObjectName").getValue();

        // get the newData from the CreateRequest object passed to this method
        Element eDeleteData = eRequest.getChild("DeleteData");
        Element eTestResult = eRequest.getChild("TestResult");
        String deleteAction = eDeleteData.getChild("DeleteAction").getAttribute("type").getValue();
        String expectedStatus = eTestResult.getAttribute("status").getValue();

        // now get whatever message object is there
        List lDeleteDataChildren = eDeleteData.getChildren(); // should only
                                                              // ever be one!
        if (lDeleteDataChildren.size() != 1) {
            // throw exception
        }
        Element eDeleteRequestObject = (Element) lDeleteDataChildren.get(1);
        if (eDeleteRequestObject == null) {
            // throw exception!
            String errMessage = "Could not find a DeleteData object in the DeleteRequest object passed in.";
            logger.fatal(errMessage);
            throw new ScheduledCommandException(errMessage);
        }

        // retrieve the message object associated to this seriesNumber from
        // AppConfig
        ActionableEnterpriseObject newJeo;
        try {
            newJeo = (ActionableEnterpriseObject) getObjectFromAppConfig(messageObjectName,
                    eRequest.getAttribute("messageRelease").getValue());
        } catch (Exception e) {
            // throw exception!
            String errMessage = "Could not find a Message Object named " + messageObjectName
                    + " in the AppConfig object associated to the TestSuite Application.  Exception: " + e.getMessage();
            throw new ScheduledCommandException(errMessage, e);
        }
        try {
            // build the message object from the createRequest.newData passed in

            // TJ 4/20/2012: apply any data that's been saved for this
            // step/object
            // from the shared TestSuiteScheduledCommand.responseMap to
            // the element that will be used to populate the returnedXeo
            applyDataToElementFromResponseMap(eDeleteRequestObject);

            newJeo.getXmlEnterpriseObject().buildObjectFromInput(eDeleteRequestObject);
        } catch (Exception e) {
            String errMessage = "Exception building JMS Message Object " + newJeo.getClass().getName()
                    + " from 'UpdateRequest/NewData' Element.  " + e.getMessage();
            throw new ScheduledCommandException(errMessage, e);
        }

        // retrieve the producer associated to this stepId from AppConfig
        PointToPointProducer p2p = getPointToPointProducer(producerName);

        // call the delete method on the message object passing the deleteAction
        // and producer
        try {
            newJeo.getXmlEnterpriseObject().setTestId(testId);

            // start timer
            long startTime = System.currentTimeMillis();

            // perform the delete request
            Result result = (Result) newJeo.delete(deleteAction, p2p);

            // stop timer and store elapsed time for averages
            sei.setElapsedTime(System.currentTimeMillis() - startTime);
            sei.setCategory(TestSuiteScheduledCommand.DELETE_ACTION);

            try {
                checkResult(result, eTestResult);
            } catch (Exception exResult) {
                throw new ScheduledCommandException(exResult.getMessage(), exResult);
            }
        } catch (Exception e) {
            // if we catch an exception, we have to check to see if the test was
            // suppose to fail,
            // if it was, this step will have passed. If not, this step has
            // failed!
            String errorFromGateway = e.getMessage();
            if (expectedStatus.equalsIgnoreCase("success")) {
                logger.fatal("The DeleteRequest failed and it was not expected!");
                throw new ScheduledCommandException(e);
            } else {
                sei.setExecutionMessage("The DeleteRequest failed but this is expected.  Reason: " + errorFromGateway);
            }
        }
        return sei;
    }

    /**
     * Publish a Delete sync message and optionally verify the sync was
     * processed properly by executing a QueryRequest.
     * <P>
     * 
     * @param producerName
     *            a String indicating the name of the producer to use to publish
     *            the sync. This can be associated to an entire TestSeries or to
     *            an individual TestStep.
     * @param testId
     *            the TestId object that contains the name of the step.
     * @param eSync
     *            the DeleteSync element obtained from the TestSuite. This may
     *            include an optional QueryRequest element that can be used to
     *            verify the sync was processed properly by the consuming
     *            gateway.
     * @return Sting a message that may contain information if the QueryRequest
     *         failed as expected. A null will be returned if the test passed
     *         and there were no expected errors.
     * @throws ScheduledCommandException
     *             if any un-expected errors occur when validating the sync with
     *             the QueryRequest. This will include any errors returned from
     *             the gateway that processed the QueryRequest.
     **/
    protected StepExecutionInfo executeDeleteSync(String producerName, TestId testId, Element eSync) throws ScheduledCommandException {

        String messageObjectName = eSync.getAttribute("messageObjectName").getValue();

        // get the newData from the CreateRequest object passed to this method
        Element eDeleteData = eSync.getChild("DeleteData");
        String deleteAction = eDeleteData.getChild("DeleteAction").getAttribute("type").getValue();

        // now get whatever message object is there
        Element eDeleteSyncObject = eDeleteData.getChild(messageObjectName);
        if (eDeleteSyncObject == null) {
            // throw exception!
            String errMessage = "Could not find a DeleteData object '" + messageObjectName + "' in the DeleteSync object passed in.";
            logger.fatal(errMessage);
            throw new ScheduledCommandException(errMessage);
        }

        // retrieve the message object associated to this seriesNumber from
        // AppConfig
        ActionableEnterpriseObject newJeo;
        try {
            newJeo = (ActionableEnterpriseObject) getObjectFromAppConfig(messageObjectName,
                    eSync.getAttribute("messageRelease").getValue());
            try {
                // build the message object from the createRequest.newData
                // passed in
                newJeo.getXmlEnterpriseObject().buildObjectFromInput(eDeleteSyncObject);
            } catch (Exception e) {
                String errMessage = "Exception building JMS Message Object " + newJeo.getClass().getName()
                        + " from 'DeleteSync/DeleteData' Element.  " + e.getMessage();
                throw new ScheduledCommandException(errMessage, e);
            }
        } catch (Exception e) {
            // throw exception!
            String errMessage = "Could not find a Message Object named " + messageObjectName
                    + " in the AppConfig object associated to the TestSuite Application.  Exception: " + e.getMessage();
            throw new ScheduledCommandException(errMessage, e);
        }

        // retrieve the producer associated to this stepId from AppConfig

        // TODO: Make this us a producer pool?
        PubSubProducer p;
        try {
            p = (PubSubProducer) getAppConfig().getObject(producerName);
        } catch (Exception e) {
            // very bad error
            String errMessage = "Could not find a Producer named " + producerName
                    + " in the AppConfig object associated to the TestSuite Application.  Exception: " + e.getMessage();
            throw new ScheduledCommandException(errMessage, e);
        }

        // call the delete method on the message object passing the deleteAction
        // and producer
        try {
            newJeo.getXmlEnterpriseObject().setTestId(testId);
            newJeo.deleteSync(deleteAction, p);
        } catch (Exception e) {
            logger.fatal(e.getMessage(), e);
            throw new ScheduledCommandException(e.getMessage(), e);
        }

        // now, if there's a QueryRequest associated to the Sync, execute it to
        // verify the sync.
        return validateSyncByQuery(testId, eSync);
    }

    /**
     * Send an Update request and validate the reply returned. Any resulting
     * Sync messages will be verified by the SyncVerificationCommand later.
     * <P>
     * 
     * @param producerName
     *            a String indicating the name of the producer to use to send
     *            the request. This can be associated to an entire TestSeries or
     *            to an individual TestStep.
     * @param testId
     *            the TestId object that contains the name of the step.
     * @param eRequest
     *            the UpdateRequest element obtained from the TestSuite.
     * @return Sting a message that may contain information if the request
     *         failed as expected. A null will be returned if the test passed
     *         and there were no expected errors.
     * @throws ScheduledCommandException
     *             if any un-expected errors occur when processing the request.
     *             This will include any errors returned from the gateway that
     *             processed the request.
     **/
    protected StepExecutionInfo executeUpdateRequest(String producerName, TestId testId, Element eRequest)
            throws ScheduledCommandException {

        StepExecutionInfo sei = new StepExecutionInfo();

        String messageObjectName = eRequest.getAttribute("messageObjectName").getValue();

        // get the newData from the CreateRequest object passed to this method
        Element eNewData = eRequest.getChild("NewData");
        Element eBaselineData = eRequest.getChild("BaselineData");
        Element eTestResult = eRequest.getChild("TestResult");
        String expectedStatus = eTestResult.getAttribute("status").getValue();

        // now get whatever message object is there
        List lNewDataChildren = eNewData.getChildren(); // should only ever be
                                                        // one!
        if (lNewDataChildren.size() != 1) {
            // throw exception
        }
        Element eNewDataObject = (Element) lNewDataChildren.get(0);
        if (eNewDataObject == null) {
            // throw exception!
            String errMessage = "Could not find a NewData object in the UpdateRequest object passed in.";
            logger.fatal(errMessage);
            throw new ScheduledCommandException(errMessage);
        }

        // now get whatever message object is there
        List lBaselineDataChildren = eBaselineData.getChildren(); // should only
                                                                  // ever be
                                                                  // one!
        if (lBaselineDataChildren.size() != 1) {
            // throw exception
            String errMessage = "Could not find any BaselineData objects in the UpdateRequest object passed in.";
            logger.fatal(errMessage);
            throw new ScheduledCommandException(errMessage);
        }
        Element eBaselineObject = (Element) lBaselineDataChildren.get(0);
        if (eBaselineObject == null) {
            // throw exception
            String errMessage = "Could not find a BaselineData object in the UpdateRequest object passed in.";
            logger.fatal(errMessage);
            throw new ScheduledCommandException(errMessage);
        }

        // retreive the message object associated to this seriesNumber from
        // AppConfig
        ActionableEnterpriseObject newJeo;
        try {
            newJeo = (ActionableEnterpriseObject) getObjectFromAppConfig(messageObjectName,
                    eRequest.getAttribute("messageRelease").getValue());
        } catch (Exception e) {
            // very bad error
            String errMessage = "Could not find a Message Object named " + messageObjectName
                    + " in the AppConfig object associated to the TestSuite Application.  Exception: " + e.getMessage();
            logger.fatal(errMessage, e);
            throw new ScheduledCommandException(errMessage, e);
        }
        XmlEnterpriseObject baselineXeo;
        try {
            baselineXeo = getObjectFromAppConfig(messageObjectName, eRequest.getAttribute("messageRelease").getValue());
        } catch (Exception e) {
            // throw exception!
            String errMessage = "Could not retrieve new/baseline objects from AppConfig.";
            logger.fatal(errMessage, e);
            throw new ScheduledCommandException(errMessage, e);
        }
        try {
            // build the message object from the createRequest.newData passed in

            // TJ 4/20/2012: apply any data that's been saved for this
            // step/object
            // from the shared TestSuiteScheduledCommand.responseMap to
            // the element that will be used to populate the returnedXeo
            applyDataToElementFromResponseMap(eNewDataObject);

            newJeo.getXmlEnterpriseObject().buildObjectFromInput(eNewDataObject);

            // TJ 4/20/2012: apply any data that's been saved for this
            // step/object
            // from the shared TestSuiteScheduledCommand.responseMap to
            // the element that will be used to populate the returnedXeo
            applyDataToElementFromResponseMap(eBaselineObject);

            baselineXeo.buildObjectFromInput(eBaselineObject);
            newJeo.getXmlEnterpriseObject().setBaseline((XmlEnterpriseObject) baselineXeo.clone());
        } catch (Exception e) {
            String errMessage = "Exception building JMS Message Object " + newJeo.getClass().getName()
                    + " from 'UpdateRequest/NewData' Element.  " + e.getMessage();
            throw new ScheduledCommandException(errMessage, e);
        }

        // retrieve the producer associated to this stepId from AppConfig
        PointToPointProducer p2p = getPointToPointProducer(producerName);

        // call the create method on the message object passing the producer
        try {
            newJeo.getXmlEnterpriseObject().setTestId(testId);

            // start timer
            long startTime = System.currentTimeMillis();

            // perform the update
            Result result = (Result) newJeo.update(p2p);

            // stop timer and store elapsed time for averages
            sei.setElapsedTime(System.currentTimeMillis() - startTime);
            sei.setCategory(TestSuiteScheduledCommand.UPDATE_ACTION);
            checkResult(result, eTestResult);
        } catch (Exception e) {
            // if we catch an exception, we have to check to see if the test was
            // suppose to fail,
            // if it was, this step will have passed. If not, this step has
            // failed!
            String errorFromGateway = e.getMessage();
            if (expectedStatus.equalsIgnoreCase("success")) {
                logger.fatal("The UpdateRequest failed and it was not expected!", e);
                throw new ScheduledCommandException(e);
            } else {
                sei.setExecutionMessage("The UpdateRequest failed but this is expected.  Reason: " + errorFromGateway);
            }
        }
        return sei;
    }

    /**
     * Publish an Update sync message and optionally verify the sync was
     * processed properly by executing a QueryRequest.
     * <P>
     * 
     * @param producerName
     *            a String indicating the name of the producer to use to publish
     *            the sync. This can be associated to an entire TestSeries or to
     *            an individual TestStep.
     * @param testId
     *            the TestId object that contains the name of the step.
     * @param eSync
     *            the UpdateSync element obtained from the TestSuite. This may
     *            include an optional QueryRequest element that can be used to
     *            verify the sync was processed properly by the consuming
     *            gateway.
     * @return Sting a message that may contain information if the QueryRequest
     *         failed as expected. A null will be returned if the test passed
     *         and there were no expected errors.
     * @throws ScheduledCommandException
     *             if any un-expected errors occur when validating the sync with
     *             the QueryRequest. This will include any errors returned from
     *             the gateway that processed the QueryRequest.
     **/
    protected StepExecutionInfo executeUpdateSync(String producerName, TestId testId, Element eSync) throws ScheduledCommandException {

        String messageObjectName = eSync.getAttribute("messageObjectName").getValue();

        // get the newData from the CreateRequest object passed to this method
        Element eNewData = eSync.getChild("NewData");
        Element eBaselineData = eSync.getChild("BaselineData");

        // now get whatever message object is there
        List lNewDataChildren = eNewData.getChildren(); // should only ever be
                                                        // one!
        if (lNewDataChildren.size() != 1) {
            // throw exception
        }
        Element eNewDataObject = (Element) lNewDataChildren.get(0);
        if (eNewDataObject == null) {
            // throw exception!
            String errMessage = "Could not find a NewData object in the UpdateSync element passed in.";
            logger.fatal(errMessage);
            throw new ScheduledCommandException(errMessage);
        }

        // now get whatever message object is there
        List lBaselineDataChildren = eBaselineData.getChildren(); // should only
                                                                  // ever be
                                                                  // one!
        if (lBaselineDataChildren.size() != 1) {
            // throw exception
            String errMessage = "Could not find any BaselineData objects in the UpdateSync element passed in.";
            logger.fatal(errMessage);
            throw new ScheduledCommandException(errMessage);
        }
        Element eBaselineObject = (Element) lBaselineDataChildren.get(0);
        if (eBaselineObject == null) {
            // throw exception
            String errMessage = "Could not find a BaselineData object in the UpdateSync element passed in.";
            logger.fatal(errMessage);
            throw new ScheduledCommandException(errMessage);
        }

        // retrieve the message object associated to this seriesNumber from
        // AppConfig
        ActionableEnterpriseObject newJeo;
        try {
            newJeo = (ActionableEnterpriseObject) getObjectFromAppConfig(messageObjectName,
                    eSync.getAttribute("messageRelease").getValue());
        } catch (Exception e) {
            // very bad error
            String errMessage = "Could not find a Message Object named " + messageObjectName
                    + " in the AppConfig object associated to the TestSuite Application.  Exception: " + e.getMessage();
            throw new ScheduledCommandException(errMessage, e);
        }

        try {
            XmlEnterpriseObject baselineXeo = getObjectFromAppConfig(messageObjectName, eSync.getAttribute("messageRelease").getValue());
            try {
                // build the message object from the createRequest.newData
                // passed in
                newJeo.getXmlEnterpriseObject().buildObjectFromInput(eNewDataObject);
                baselineXeo.buildObjectFromInput(eBaselineObject);
                newJeo.getXmlEnterpriseObject().setBaseline((XmlEnterpriseObject) baselineXeo.clone());
            } catch (Exception e) {
                String errMessage = "Exception building JMS Message Object " + newJeo.getClass().getName()
                        + " from 'UpdateRequest/NewData' Element.  " + e.getMessage();
                throw new ScheduledCommandException(errMessage, e);
            }
        } catch (Exception e) {
            // throw exception!
            String errMessage = "Could not retrieve new/baseline objects from AppConfig.";
            logger.fatal(errMessage, e);
            throw new ScheduledCommandException(errMessage, e);
        }

        // retrieve the producer associated to this stepId from AppConfig
        PubSubProducer p;
        try {
            p = (PubSubProducer) getAppConfig().getObject(producerName);
        } catch (Exception e) {
            // very bad error
            String errMessage = "Could not find a Producer named " + producerName
                    + " in the AppConfig object associated to the TestSuite Application.  Exception: " + e.getMessage();
            throw new ScheduledCommandException(errMessage, e);
        }

        // call the create method on the message object passing the producer
        try {
            newJeo.getXmlEnterpriseObject().setTestId(testId);
            newJeo.updateSync(p);
        } catch (Exception e) {
            logger.fatal(e.getMessage(), e);
            throw new ScheduledCommandException(e.getMessage(), e);
        }

        // now, if there's a QueryRequest associated to the Sync, execute it to
        // verify the sync.
        return validateSyncByQuery(testId, eSync);
    }

    /**
     * Send a Query request and validate the reply returned.
     * <P>
     * 
     * @param producerName
     *            a String indicating the name of the producer to use to send
     *            the request. This can be associated to an entire TestSeries or
     *            to an individual TestStep.
     * @param testId
     *            the TestId object that contains the name of the step.
     * @param eRequest
     *            the QueryRequest element obtained from the TestSuite.
     * @return Sting a message that may contain information if the request
     *         failed as expected. A null will be returned if the test passed
     *         and there were no expected errors.
     * @throws ScheduledCommandException
     *             if any un-expected errors occur when processing the request.
     *             This will include any errors returned from the gateway that
     *             processed the request.
     **/
    protected StepExecutionInfo executeQueryRequest(String producerName, TestId testId, Element eRequest) throws ScheduledCommandException {

        StepExecutionInfo sei = new StepExecutionInfo();

        String messageObjectName = eRequest.getAttribute("messageObjectName").getValue();
        String messageRelease = eRequest.getAttribute("messageRelease").getValue();
        Attribute aQueryObject = eRequest.getAttribute("queryObjectName");

        String queryObjectName;
        if (aQueryObject != null) {
            queryObjectName = aQueryObject.getValue();
        } else {
            queryObjectName = "LightweightPerson";
        }

        // get the newData from the CreateRequest object passed to this method
        Element eQueryData = eRequest.getChild("QueryData");
        Element eTestResult = eRequest.getChild("TestResult");
        Element eProvideData = eRequest.getChild("ProvideData");
        String expectedStatus = eTestResult.getAttribute("status").getValue();

        // now get the query message object
        List lQueryChildren = eQueryData.getChildren(); // should only ever be
                                                        // one!
        if (lQueryChildren.size() != 1) {
            // throw exception
            String errMessage = "No query objects found in QueryRequest element passed in.";
            logger.fatal(errMessage);
            throw new ScheduledCommandException(errMessage);
        }
        Element eQueryObject = (Element) lQueryChildren.get(0);
        if (eQueryObject == null) {
            // throw exception!
            String errMessage = "Could not find a Query object in the QueryRequest object passed in.";
            logger.fatal(errMessage);
            throw new ScheduledCommandException(errMessage);
        }

        // these are the expected provide objects
        List lProvideDataChildren = eProvideData.getChildren();

        // prime the list of expected objects that we expect to be returned.
        List<XmlEnterpriseObject> provideXeos = new ArrayList<>();
        for (int i = 0; i < lProvideDataChildren.size(); i++) {
            Element eProvideDataObject = (Element) lProvideDataChildren.get(i);
            XmlEnterpriseObject xeo = getObjectFromAppConfig(messageObjectName, messageRelease);

            try {
                // TJ 4/20/2012: apply any data that's been saved for this
                // step/object
                // from the shared TestSuiteScheduledCommand.responseMap to
                // the element that will be used to populate the xeo
                applyDataToElementFromResponseMap(eProvideDataObject);

                // build the xeos that are expected in the provide-reply
                xeo.buildObjectFromInput(eProvideDataObject);

                provideXeos.add(xeo);
            } catch (Exception e) {
                String errMessage = "Exception building Provide Expected result object " + xeo.getClass().getName()
                        + " from 'ProvideData' Element.  " + e.getMessage();
                throw new ScheduledCommandException(errMessage, e);
            }

        }

        // retrieve the query object from app config
        XmlEnterpriseObject queryObject = getObjectFromAppConfig(queryObjectName, messageRelease);

        try {
            // TJ 4/20/2012: apply any data that's been saved for this
            // step/object
            // from the shared TestSuiteScheduledCommand.responseMap to
            // the element that will be used to populate the queryObject
            applyDataToElementFromResponseMap(eQueryObject);
            logger.info("eQueryObject=" + new XMLOutputter().outputString(eQueryObject));

            // build the message object from the query object element (from the
            // suite)
            queryObject.buildObjectFromInput(eQueryObject);

        } catch (Exception e) {
            String errMessage = "Exception building Query Object " + queryObject.getClass().getName()
                    + " from 'QueryRequest/QueryData' Element.  " + e.getMessage();
            throw new ScheduledCommandException(errMessage, e);
        }

        // retrieve the producer associated to this stepId from AppConfig
        PointToPointProducer p2p = getPointToPointProducer(producerName);

        // call the query method on the message object passing the producer
        ActionableEnterpriseObject queryJeo;
        try {
            queryJeo = (ActionableEnterpriseObject) getObjectFromAppConfig(messageObjectName,
                    eRequest.getAttribute("messageRelease").getValue());
        } catch (Exception e) {
            // very bad error
            String errMessage = "Exception occurred querying.  Exception: " + e.getMessage();
            throw new ScheduledCommandException(errMessage, e);
        }
        try {
            queryJeo.getXmlEnterpriseObject().setTestId(testId);

            // start the timer
            long startTime = System.currentTimeMillis();

            // perform the query
            List v = queryJeo.query(queryObject, p2p);

            // stop timer and store elapsed time for averages
            sei.setElapsedTime(System.currentTimeMillis() - startTime);
            sei.setCategory(TestSuiteScheduledCommand.QUERY_ACTION);

            if (v.size() != provideXeos.size()) {
                String errMessage = "Incorrect number of message objects returned in provide.  Got " + v.size() + " expected "
                        + provideXeos.size();

                throw new ScheduledCommandException(errMessage);
            }

            for (int i = 0; i < v.size(); i++) {
                XmlEnterpriseObject returnedXeo = (XmlEnterpriseObject) v.get(i);

                // TJ - 12/6/2011
                returnedXeo = applyTransformation(eProvideData.getAttributeValue(XSL_URI), returnedXeo);
                // END TJ - 12/6/2011

                // check to see if the returnedXeo exists in the expected
                // Provide xeo list
                boolean foundReturnedXeo = false;
                XmlEnterpriseObject expectedXeo = null;
                for (int j = 0; j < provideXeos.size(); j++) {
                    expectedXeo = provideXeos.get(j);
                    if (expectedXeo.equals(returnedXeo)) {
                        foundReturnedXeo = true;
                        break;
                    }
                }
                if (!foundReturnedXeo) {
                    String errMessage = " Mismatch: " + "\n\nExpected=" + expectedXeo.toXmlString() + "\n\nReturned="
                            + returnedXeo.toXmlString() + "\n\ndiff="
                            + StringUtils.difference(expectedXeo.toXmlString(), returnedXeo.toXmlString());
                    throw new ScheduledCommandException(errMessage);
                }
            }
        } catch (NoClassDefFoundError e) {
            logger.fatal(e);
            // rethrow the error did not terminate the application nor showing
            // up anything unusual, essentially just hangs over there
            throw new RuntimeException(e);
        } catch (Exception e) {
            // if we catch an exception, we have to check to see if the test was
            // suppose to fail,
            // if it was, this step will have passed. If not, this step has
            // failed!
            if (expectedStatus.equalsIgnoreCase("success")) {
                logger.fatal("The QueryRequest failed and it was not expected!");

                throw new ScheduledCommandException(e);
            } else {
                sei.setExecutionMessage("The QueryRequest failed but this is expected.  Reason: " + e.getMessage());
            }
        }
        logger.debug(LOGTAG + "return");

        return sei;
    }

    private void addObjectToResponseMap(String stepId, XmlEnterpriseObject xeo) {

        String responseMapKey = stepId + "." + xeo.getClass().getSimpleName();
        logger.info("Adding " + responseMapKey + " to TestSuiteScheduledCommand.responseMap.");
        TestSuiteScheduledCommand.responseMap.put(responseMapKey, xeo);
    }

    private static String getNestedValueFromObject(String value, XmlEnterpriseObject xeo)
            throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {

        String retVal = "";

        // - use the string passed in to build a method call(s) (getters)
        // - iterate through the list of getter methods and invoke them all
        // - once we find one that returns a string, return it

        String className = xeo.getClass().getSimpleName();
        String methodCall = value.substring(value.indexOf(className + ".") + className.length() + 1);
        methodCall = methodCall.replaceAll("\\(\\)", "");
        logger.info("Nested method call is: " + methodCall);

        String[] methodNames = methodCall.split("\\.");
        logger.info("There are " + methodNames.length + " methods to invoke.");
        Object currentObject = xeo;
        for (String method : methodNames) {
            Method getter = currentObject.getClass().getMethod(method, new Class[] {});
            logger.info("Invoking method " + currentObject.getClass().getSimpleName() + "." + method + "()");
            Object obj = getter.invoke(currentObject, new Object[] {});
            if (obj instanceof String) {
                retVal = (String) obj;
                logger.info("Value to return is: '" + retVal + "'");
            } else {
                logger.info("Value returned is a " + obj.getClass().getSimpleName() + " continuing to next " + "method call.");
                currentObject = obj;
            }
        }

        return retVal;
    }

    protected static boolean applyDataToElementFromResponseMap(Element eSourceElement)
            throws SecurityException, IllegalArgumentException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {

        boolean dataApplied = false;

        Set<String> responseMapKeys = TestSuiteScheduledCommand.responseMap.keySet();

        // for each key

        // for each child attribute in eSourceElement
        // if attribute value starts with key
        // - get object from map
        // - call this.getValueFromObject passing method call part of attribute
        // value and the object
        // - set attribute value to the value returned
        // - set dataApplied to true

        // for each child element in eSourceElement
        // if element has children
        // - call this method again passing child element
        // else
        // - if element value starts with key
        // - get object from map
        // - call this.getValueFromObject passing method call part of attribute
        // value and the object
        // - set element value to the value returned
        // - set dataApplied to true

        Iterator<String> responseMapKeyIterator = responseMapKeys.iterator();
        while (responseMapKeyIterator.hasNext()) {
            String key = responseMapKeyIterator.next();

            List attrs = eSourceElement.getAttributes();
            for (int i = 0; i < attrs.size(); i++) {
                Attribute a = (Attribute) attrs.get(i);
                String value = a.getValue();
                if (value.startsWith(key)) {
                    logger.info("Found an attribute that uses data from a previous step: " + key);
                    XmlEnterpriseObject xeo = TestSuiteScheduledCommand.responseMap.get(key);
                    String valueFromObject = getNestedValueFromObject(value, xeo);
                    a.setValue(valueFromObject);
                    dataApplied = true;
                }
            }

            List elements = eSourceElement.getChildren();
            for (int i = 0; i < elements.size(); i++) {
                Element e = (Element) elements.get(i);
                if (e.getChildren().size() > 0) {
                    applyDataToElementFromResponseMap(e);
                } else {
                    String eText = e.getText();
                    if (eText.startsWith(key)) {
                        logger.info("Found an element ('" + e.getName() + "') " + "that uses data from a previous step: " + key);
                        XmlEnterpriseObject xeo = TestSuiteScheduledCommand.responseMap.get(key);
                        String valueFromObject = getNestedValueFromObject(eText, xeo);
                        e.setText(valueFromObject);
                        dataApplied = true;
                    }
                }
            }
        }
        return dataApplied;
    }

    private XmlEnterpriseObject getObjectFromAppConfig(String messageObjectName, String messageRelease) throws ScheduledCommandException {
        XmlEnterpriseObject xeo;
        try {
            xeo = (XmlEnterpriseObject) getAppConfig().getObject(messageObjectName + "." + generateRelease(messageRelease));
        } catch (Exception e) {
            try {
                xeo = (XmlEnterpriseObject) getAppConfig().getObject(messageObjectName);
            } catch (Exception e2) {
                // very bad error
                String errMessage = "Could not find a Message Object named " + messageObjectName
                        + " in the AppConfig object associated to the TestSuite Application.  Exception: " + e.getMessage();
                throw new ScheduledCommandException(errMessage, e);
            }
        }
        return xeo;
    }

    private String generateRelease(String release) {
        if (release != null) {
            return "v" + release.replace('.', '_');
        } else {
            return null;
        }
    }

    /**
     * This method queries for an object using the information found in the
     * QueryData element associated to the request and then turns around and
     * deletes all objects returned in that query. It's NOT a typical test
     * case/step that will be verified and summarized as part of the overall
     * test suite. The query and subsequent delete calls either work or they
     * don't. It will log those errors but not report on them in the test suite
     * summary document (at least not at this point).
     **/
    protected StepExecutionInfo executeTearDownRequest(String producerName, TestId testId, Element eRequest)
            throws ScheduledCommandException {

        StepExecutionInfo sei = new StepExecutionInfo();

        String messageObjectName = eRequest.getAttribute("messageObjectName").getValue();
        Attribute aQueryObject = eRequest.getAttribute("queryObjectName");

        String queryObjectName;
        if (aQueryObject != null) {
            queryObjectName = aQueryObject.getValue();
        } else {
            queryObjectName = "LightweightPerson";
        }

        // get the newData from the CreateRequest object passed to this method
        Element eQueryData = eRequest.getChild("QueryData");
        Element eDeleteAction = eRequest.getChild("DeleteAction");
        String deleteAction = eDeleteAction.getAttribute("type").getValue();

        // now get the query message object
        List lQueryChildren = eQueryData.getChildren(); // should only ever be
                                                        // one!
        if (lQueryChildren.size() != 1) {
            // throw exception
            String errMessage = "No query objects found in TearDownRequest element passed in.";
            logger.fatal(errMessage);
            throw new ScheduledCommandException(errMessage);
        }
        Element eQueryObject = (Element) lQueryChildren.get(0);
        if (eQueryObject == null) {
            // throw exception!
            String errMessage = "Could not find a Query object in the TearDownRequest object passed in.";
            logger.fatal(errMessage);
            throw new ScheduledCommandException(errMessage);
        }

        // retreive the query object from app config
        XmlEnterpriseObject queryObject;
        try {
            queryObject = getObjectFromAppConfig(messageObjectName, eRequest.getAttribute("messageRelease").getValue());
            try {
                // build the message object from the createRequest.newData
                // passed in
                queryObject.buildObjectFromInput(eQueryObject);
            } catch (Exception e) {
                String errMessage = "Exception building Query Object " + queryObject.getClass().getName()
                        + " from 'TearDownRequest/QueryData' Element.  " + e.getMessage();
                throw new ScheduledCommandException(errMessage, e);
            }
        } catch (Exception e) {
            // very bad error
            String errMessage = "Could not find a Query Object named " + queryObjectName
                    + " in the AppConfig object associated to the TestSuite Application.  Exception: " + e.getMessage();
            throw new ScheduledCommandException(errMessage, e);
        }

        // retrieve the producer associated to this stepId from AppConfig
        PointToPointProducer p2p = getPointToPointProducer(producerName);

        // call the query method on the message object and delete all returned
        // objects
        try {
            ActionableEnterpriseObject queryJeo = (ActionableEnterpriseObject) getObjectFromAppConfig(messageObjectName,
                    eRequest.getAttribute("messageRelease").getValue());
            purgeObject(deleteAction, queryJeo, queryObject, p2p);
        } catch (Exception e) {
            logger.fatal(e.getMessage(), e);
            sei.setExecutionMessage(e.getMessage());
        }

        return sei;
    }

    protected void purgeObject(String deleteAction, ActionableEnterpriseObject queryJeo, XmlEnterpriseObject queryObject,
            PointToPointProducer p2p) throws ScheduledCommandException {
        List v;
        try {
            v = queryJeo.query(queryObject, p2p);
            logger.info("Cleaning up [" + v.size() + "] " + queryJeo.getClass().getName() + " objects...");
        } catch (Exception e) {
            String errMessage = "Exception querying for the " + queryJeo.getClass().getName() + " we need to tear down: " + e.getMessage();
            logger.fatal(errMessage);
            throw new ScheduledCommandException(e.getMessage(), e);
        }

        if (v != null) {
            for (int i = 0; i < v.size(); i++) {
                ActionableEnterpriseObject returnedJeo = (ActionableEnterpriseObject) v.get(i);

                // now delete the object at the destination...
                try {
                    returnedJeo.delete(deleteAction, p2p);
                    logger.info("Tore down the " + returnedJeo.getClass().getName() + " object.");
                } catch (Exception e) {
                    String errMessage = "Exception deleting the " + returnedJeo.getClass().getName() + " we need to tear down: "
                            + e.getMessage();
                    logger.fatal(errMessage);
                    throw new ScheduledCommandException(e.getMessage(), e);
                }
            }
        }
    }

    protected AppConfig getAppConfig() {
        return m_appConfig;
    }

    private PointToPointProducer getPointToPointProducer(String producerName) throws ScheduledCommandException {
        try {
            /*
             * TODO: Make this us a producer pool? ProducerPool pool =
             * (ProducerPool) getAppConfig().getObject(producerName);
             * PointToPointProducer p2p = (PointToPointProducer)
             * pool.getProducer();
             */
            PointToPointProducer p2p = (PointToPointProducer) getAppConfig().getObject(producerName);
            p2p.setRequestTimeoutInterval(m_producerTimeoutInterval);
            return p2p;
        } catch (Exception e) {
            // very bad error
            String errMessage = "Could not find a Producer named " + producerName
                    + " in the AppConfig object associated to the TestSuite Application.  Exception: " + e.getMessage();
            throw new ScheduledCommandException(errMessage, e);
        }
    }

    protected void checkResult(Result result, Element expectedResult) throws ScheduledCommandException {
        Result expectedResultObject = new Result();

        try {
            expectedResult.addContent((Element) result.getProcessedMessageId().buildOutputFromObject());
            expectedResultObject.buildObjectFromInput(expectedResult);
            if (!expectedResultObject.equals(result)) {
                throw new ScheduledCommandException("Result objects are not equal:  " + "\n" + result.toXmlString());
            }
        } catch (EnterpriseLayoutException | XmlEnterpriseObjectException exp) {
            logger.fatal("Error checking result", exp);
            throw new ScheduledCommandException(exp.getMessage(), exp);
        }
    }
}
