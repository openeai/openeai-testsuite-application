/**********************************************************************
 This file is part of the OpenEAI sample, reference implementation,
 and deployment management suite created by Tod Jackson
 (tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
 the University of Illinois Urbana-Champaign.

 Copyright (C) 2002 The OpenEAI Software Foundation

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 For specific licensing details and examples of how this software
 can be used to implement integrations for your enterprise, visit
 http://www.OpenEai.org/licensing.
 */

package org.openeai.implementations.applications.testsuite;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;
import org.openeai.afa.ScheduledCommand;
import org.openeai.afa.ScheduledCommandException;
import org.openeai.afa.ScheduledCommandImpl;
import org.openeai.config.CommandConfig;
import org.openeai.config.EnterpriseConfigurationObjectException;
import org.openeai.config.EnterpriseFieldException;
import org.openeai.config.LoggerConfig;
import org.openeai.config.PropertyConfig;
import org.openeai.jms.consumer.PubSubConsumer;
import org.openeai.jms.consumer.commands.CommandException;
import org.openeai.jms.producer.PointToPointProducer;
import org.openeai.moa.ActionableEnterpriseObject;
import org.openeai.moa.XmlEnterpriseObject;
import org.openeai.moa.objects.resources.Datetime;
import org.openeai.moa.objects.testsuite.Failure;
import org.openeai.moa.objects.testsuite.MaximumTime;
import org.openeai.moa.objects.testsuite.MinimumTime;
import org.openeai.moa.objects.testsuite.RequestTimings;
import org.openeai.moa.objects.testsuite.TestCaseSummary;
import org.openeai.moa.objects.testsuite.TestId;
import org.openeai.moa.objects.testsuite.TestSeriesSummary;
import org.openeai.moa.objects.testsuite.TestStatus;
import org.openeai.moa.objects.testsuite.TestStepSummary;
import org.openeai.moa.objects.testsuite.TestSuiteSummary;
import org.openeai.threadpool.ThreadPool;
import org.openeai.xml.XmlDocumentReader;

import java.io.File;
import java.io.FileOutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * This is the first of two commands that make up the OpenEAI "TestSuite"
 * Application. This command is a ScheduledCommand that reads a TestSuite XML
 * document, performs the messaging actions specified and validates the results
 * of those actions. It is simply a messaging application that uses OpenEAI
 * foundation APIs to interact with a gateway developed on the OpenEAI
 * foundation.
 * <P>
 * Based on the contents of the TestSuite document this command will create,
 * update and delete objects at the destination. It will also query the
 * destination to verify create, delete and update actions were successful. The
 * TestSuite document specifies all the expected results that this command uses
 * to verify actions performed.
 * <P>
 * This command works in conjunction with the SyncVerificationCommand to perform
 * and verify theses tasks. It performs all the Point-To-Point messaging and the
 * SyncVerificationCommand consumes the resulting Synchronization messages that
 * should be published by an authoritative source when the Point-To-Point
 * actions are executed. i.e. - when a create-request is sent to an
 * authoritative source, that system should publish a create-sync message
 * according to the OpenEAI Message Protocol.
 * <P>
 * Additionally, this command can publish Sync messages <b>to</b> a
 * non-authoritative system and can verify that system processed those syncs
 * properly by querying that system for whatever it is authoritative for that
 * may include information from the message objects it's not authoritative for.
 * <P>
 * For example, a system may consume BasicPerson-Create-Sync messages to keep
 * it's own store up-to-date. It <b>IS NOT</b> authoritative for BasicPerson
 * objects. However, it <b>IS</b> authoritative for some other object that
 * includes some BasicPerson information. Therefore, it consumes BasicPerson
 * sync messages and when someone queries for the object it is authoritative
 * for, it uses some of that BasicPerson information in that object. If that
 * system isn't authoritative for anything, this command can also be used to
 * simply publish sync messages to that system and the processing of those synce
 * messages would have to be verified manually or by some other means.
 * <P>
 * <B>Relations to the TestSuite document</B>
 * <P>
 * As stated above, this command reads a TestSuite document and executes the
 * instructions specified in that document. In order to do that though, there
 * are some relationships that must be specified between that document and this
 * command. These are listed here:
 * <ul>
 * <li>The PointToPoint producer named in the TestSuite document (producerName
 * attribute associated to a TestSeries) must be specified in the Configuration
 * associated to this Command. The name of the producer must match the name
 * specified in the TestSuite document and must be configured appropriately to
 * produce requests to the gateway being tested.
 * <li>Any message object used in the TestSuite, must be specified and
 * configured appropriately in the Configuration associated to this Command. For
 * example, if the TestSuite specifies that a BasicPerson object is used to
 * perform BasicPerson/Create-Requests, that object must be configured in the
 * MessageObjectConfigs section of the deployment descriptor associated to this
 * command. Additionally, that message object must be named according to the
 * name given to it in the CreateRequest element of the TestSuite document. This
 * command will look for that named message object in it's AppConfig with which
 * to perform the requests specified in the TestSuite document. The message
 * objects used in performing actions such as create, delete and update will be
 * specified in the "messageObjectName" attribute associated to those actions
 * and must be included in this Command's Configuration element. Likewise,
 * objects used to perform queries will be specified in the TestSuite document
 * by the "queryObjectName" attribute and must be included in this Command's
 * Configuration.
 * </ul>
 * <P>
 * <B>Test Suite helper Objects</B>
 * <P>
 * In order to correlate any request action to a reply or to a sync message
 * published as a result of that action, the following Message Objects must be
 * specified in this command's Configuration:
 * <ul>
 * <li>org.openeai.moa.objects.testsuite.TestId
 * <li>org.openeai.moa.objects.testsuite.TestStatus
 * <li>org.openeai.moa.objects.testsuite.TestSuiteSummary
 * <li>org.openeai.moa.objects.testsuite.TestSeriesSummary
 * <li>org.openeai.moa.objects.testsuite.TestCaseSummary
 * <li>org.openeai.moa.objects.testsuite.TestStepSummary
 * </ul>
 * These objects are a part of the OpenEAI JAR file (openeai.jar) and are used
 * to complete the TestSuite summary document as well as correlate replies and
 * sync messages to the requests that precipitated them. The name of the objects
 * should should match their class name (without the package).
 * <P>
 * Gateways that are tested with this Test Suite Application should pull the
 * TestId out of the ControlAreaRequest/Sender element and populate the
 * synchronization messages it publishes with that information.
 * <P>
 * <B>Consuming the Sync Messages:</B>
 * <P>
 * In order to complete the TestSuite, the TestSuite Application must produce
 * requests, consume the replies, verify those replies and consume any
 * synchronization messages that result from the request-reply actions. To do
 * this, this scheduled application actually contains a PubSubConsumer to
 * consume the sync messages. Therefore, there must be at least one
 * PubSubConsumer configured within this Command's Configuration Element that is
 * set up appropriately to consume these sync messages.
 * <P>
 * <B>The TestSuiteScheduledCommand (this command) expects any consumer that is
 * to serve this purpose to have a name that starts with "PubSubConsumer".</B>
 * <P>
 * This command will retrieve any consumer with a name staring the
 * "PubSubConsumer" from its AppConfig, start them to verify their durable
 * subscriptions have been established and allow them to consume any message
 * left over from previous runs. Then it will stop them, and perform all the
 * request-reply messaging before re-starting the consumer(s) to process the
 * sync messages that should have been published during the request-reply
 * activity. Between the request-reply message production and verification and
 * the consumption and verificaton of resulting sync messages, the entire
 * TestSuite is completed.
 * <P>
 * <B>Configuration Parameters:</B>
 * <P>
 * These are the configuration parameters associated to this command.
 * <P>
 * <TABLE BORDER=2 CELLPADDING=5 CELLSPACING=2>
 * <TR>
 * <TH>Property Name</TH>
 * <TH>Required</TH>
 * <TH>Description</TH>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>TestSuiteDocUri</TD>
 * <TD>yes</TD>
 * <TD>The URL for the actual TestSuite associated to this instance of the
 * TeseSuiteApplication. This is the same value used by the
 * SyncVerificationCommand that points to the TestSuite document used by both
 * commands to compare expected with actual results. The TestSuite document
 * contains all the instructions that are to be carried out by this command as
 * well as the expected sync messages that the SyncVerificationCommand expects
 * to consume.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>TestSuiteSummaryDocPath</TD>
 * <TD>yes</TD>
 * <TD>This is the output path (directory name) where the TestSuite summary
 * document should be written to. Both the SyncVerificationCommand and this
 * command need this parameter. They will both write the results of their
 * processing to a file in this directory. The name of the summary document will
 * be the actual file name of the test suite document with the word "-summary"
 * appdended to it. For example, if the TestSuiteDocUri is:
 * http://xml.openeai.org
 * /xml/configs/messaging/environments/development/tests/1.0
 * /TestSuite_BasicPerson_v1_0.xml then the summary document name would be
 * TestSuite_BasicPerson_v1_0-summary.xml and it would be written to the
 * directory specified in this property (e.g. "./").
 * <P>
 * If this command recognizes that a sync message should be published based on
 * an action performed, it will set the status of that test step to 'pending'
 * (unless a failure occurs carrying out that action). The
 * SyncVerificationCommand will set the status to 'success' or 'failure' based
 * on the results of it's processing when it consumes the sync message
 * associated to the action.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>producerTimeoutInterval</TD>
 * <TD>no (default=30000 or 30 seconds)</TD>
 * <TD>This is the maximum time (specified in milliseconds) that the producer
 * used to carry out the requests listed in the TestSuite will wait for a
 * response. If the producer does not receive a response to the request it sends
 * within this time, it will throw an exception resulting in a 'failure'. This
 * property can be used to benchmark a gateway being tested to see how big of a
 * load can be thrown at it before performance starts to degrade. This can be
 * useful in determining such things as how many gateways to deploy etc. because
 * once you start causing timeouts due to a massive load, you can see how adding
 * gateways to a cluster affects those errors.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>maxProcessTime</TD>
 * <TD>no (default=300000 or 5 minutes)</TD>
 * <TD>This is the maximum time (specified in milliseconds) that the entire
 * application will stay alive. This value is only used if the
 * SyncVerificationCommand never starts consuming messages as a result of
 * actions performed by this command. Once the SyncVerificationCommand has
 * consumed and processed its first message, the 'maxConsumptionTime' property
 * associated to it will be the only relavant "timer" for the application.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>useThreads (true | false)</TD>
 * <TD>no (default=false)</TD>
 * <TD>This property tells the TestSuiteScheduledCommand whether or not it
 * should use threads to execute the test suite. Each TestSeries in the suite
 * can be executed in Threads. <B>If threads are to be used, this command
 * expects there to be a ThreadPoolConfig associated to it named
 * "TestSuiteThreadPool".</B></TD>
 * </TR>
 * </TABLE>
 * 
 * @author Tod Jackson
 * @see SyncVerificationCommand
 */
public class TestSuiteScheduledCommand extends ScheduledCommandImpl implements ScheduledCommand {
    private static Logger logger = Logger.getLogger(TestSuiteScheduledCommand.class);
    private static String LOGTAG = "[" + TestSuiteScheduledCommand.class.getSimpleName() + "]";

    private Document m_testSuiteDoc = null;
    private HashMap m_failedSteps = new HashMap();
    private HashMap m_passedSteps = new HashMap();
    private Document m_testSuiteSummaryDoc = null;
    private String m_testSuiteSummaryDocPath = "";
    private String m_testSuiteSummaryDocFileName = "";
    private CommandConfig m_commandConfig = null;

    private long m_maxProcessTime = 300000; // maximum number of milliseconds
                                            // that we want this process to stay
                                            // alive
    private java.util.Date m_processStartTime = null;
    private Datetime m_startTime = null;
    private Datetime m_endTime = null;

    private int m_expectedNumberOfSyncs = 0;
    private int cnt_totalSeries = 0;
    private int cnt_passedSeries = 0;
    private int cnt_failedSeries = 0;

    private int cnt_totalCases = 0;
    private int cnt_passedCases = 0;
    private int cnt_failedCases = 0;

    private int cnt_totalSteps = 0;
    private int cnt_passedSteps = 0;
    private int cnt_failedSteps = 0;

    public static boolean STAY_ALIVE = true;
    public final static String GENERATE_ACTION = "Generate";
    public final static String CREATE_ACTION = "Create";
    public final static String DELETE_ACTION = "Delete";
    public final static String QUERY_ACTION = "Query";
    public final static String UPDATE_ACTION = "Update";
    // TJ 4/20/2012 - added
    public static HashMap<String, XmlEnterpriseObject> responseMap = new HashMap<String, XmlEnterpriseObject>();
    public static List<String> stepIds = new java.util.ArrayList<String>();

    private ArrayList m_receivers = new ArrayList();

    private HashMap m_timings = new HashMap();

    /*
     * private ArrayList m_createTimes = new ArrayList(); private ArrayList
     * m_deleteTimes = new ArrayList(); private ArrayList m_updateTimes = new
     * ArrayList(); private ArrayList m_queryTimes = new ArrayList();
     */

    private HashMap m_maxRequestTimes = new HashMap();
    private HashMap m_minRequestTimes = new HashMap();

    /**
     * This is the constructor used by the ScheduledApp foundation to
     * instantiate this ScheduledCommand. The ScheduleApp foundation will
     * instantiate this command passing the commands configuration information
     * when the application is started.
     * <P>
     * The following variables associated to this command are initialized in
     * this constructor:
     * 
     * <P>
     * 
     * @param cConfig
     *            the commands configuration information specified in the
     *            applications deployment document.
     **/
    public TestSuiteScheduledCommand(CommandConfig cConfig) throws InstantiationException {
        super(cConfig);

        m_commandConfig = cConfig;

        try {
            LoggerConfig lConfig = new LoggerConfig();
            lConfig = (LoggerConfig) getAppConfig().getObjectByType(lConfig.getClass().getName());
            logger = Logger.getLogger(TestSuiteScheduledCommand.class);
            PropertyConfigurator.configure(lConfig.getProperties());
        } catch (Exception e) {
        }

        try {
            PropertyConfig pConfig = (PropertyConfig) getAppConfig().getObject("TestSuiteProperties");
            setProperties(pConfig.getProperties());

            m_maxProcessTime = Integer.parseInt(getProperties().getProperty("maxProcessTime", "300000"));

            XmlDocumentReader xmlReader = new XmlDocumentReader();
            String testSuiteDocUri = getProperties().getProperty("TestSuiteDocUri");
            // make relative path work george wang 3/16/2012
            String fileName = docUriToFileName(testSuiteDocUri);
            logger.info("fileName=" + fileName);
            m_testSuiteDoc = xmlReader.initializeDocument(fileName, getInboundXmlValidation());
            if (m_testSuiteDoc == null) {
                throw new InstantiationException("Missing 'TestSuiteDocUri' property in configuration document.  Can't continue.");
            }
            // get a copy of the testsuite document to use as the status
            // document.
            // we'll add teststatuses to this document and ultimately serialize
            // it to
            // an xml file.

            m_testSuiteSummaryDocPath = getProperties().getProperty("TestSuiteSummaryDocPath", "./");
            m_testSuiteSummaryDoc = (Document) m_testSuiteDoc.clone();
            m_testSuiteSummaryDocFileName = m_testSuiteSummaryDocPath
                    + fileName.substring(fileName.lastIndexOf("/") + 1, fileName.indexOf(".xml")) + "-summary.xml";
            logger.info("TestSuite Summary file name is: " + m_testSuiteSummaryDocFileName);

            m_expectedNumberOfSyncs = getExpectedNumberOfSyncs();
            m_processStartTime = new java.util.Date();
        } catch (Exception e) {
            logger.fatal("Error initializing 'TestSuiteScheduledCommand' can't continue.");
            logger.fatal(e.getMessage(), e);
            throw new InstantiationException(e.getMessage());
        }

        try {
            gatherReceivers();
            startCleanup();
        } catch (Exception e) {
            logger.fatal("Error initializing receivers can't continue.");
            logger.fatal(e.getMessage(), e);
            throw new InstantiationException(e.getMessage());
        }

        // we need to start and stop the consumers to establish the Durable
        // subscriptions
        // incase this is the first time we've ran the test suite application...

        // first, we'll tell the SyncVerificationCommand to NOT process any
        // messages that
        // might have been left over from previous runs.
        if (m_expectedNumberOfSyncs > 0) {
            logger.info("Starting Consumers to verify durable subscription is established.");
            SyncVerificationCommand.PROCESS_MESSAGES = false;
            try {
                java.util.List pubSubConsumers = getAppConfig().getObjectsLike("PubSubConsumer");
                for (int i = 0; i < pubSubConsumers.size(); i++) {
                    PubSubConsumer pubSubConsumer = (PubSubConsumer) pubSubConsumers.get(i);
                    try {
                        if (pubSubConsumer.isStarted() == false) {
                            // to establish the durable subscription, in case
                            // they haven't
                            // been established before
                            pubSubConsumer.startConsumer();
                        }
                        try {
                            logger.info("Waiting for SyncVerificationCommand to clean up messages left over from previous runs...");
                            Thread.sleep(2000); // give consumer a chance to
                                                // start consuming left over
                                                // messages
                            boolean keepWaiting = true;
                            while (keepWaiting) {
                                Thread.sleep(1000);
                                if (System.currentTimeMillis() - SyncVerificationCommand.LAST_CLEANUP_TIME.getTime() > 3000) {
                                    keepWaiting = false;
                                }
                            }
                        } catch (Exception e) {
                        }
                        Thread.sleep(2000);
                        logger.info("SyncVerificationCommand is finished cleaning up messages left over from previous runs.");
                        pubSubConsumer.stopMonitor(); // keeps consumer Monitor
                                                      // from starting it back
                                                      // up.
                        pubSubConsumer.stopConsumer(); // actually disconnects
                                                       // the consumer from it's
                                                       // topic etc.
                    } catch (Exception e) {
                        String errMessage = "Exception occurred starting consumer " + pubSubConsumer.getConsumerName() + "  Exception: "
                                + e.getMessage();
                        logger.fatal(errMessage, e);
                    }
                }
            } catch (Exception e) {
                // very bad error
            }
        }

        try {
            finishCleanup();
        } catch (CommandException e) {
            logger.fatal("Error initializing receivers can't continue.");
            logger.fatal(e.getMessage(), e);
            throw new InstantiationException(e.getMessage());
        }

        logger.info("Consumers have been stopped so NO message consumption will "
                + "occur until the TestSuiteApplication has completed the PointToPoint portions of the tests.");

        logger.info("TestSuiteScheduledCommand, instantiated successfully.");
    }

    public static String docUriToFileName(String testSuiteDocUri) throws MalformedURLException {
        String fileName;
        if (testSuiteDocUri.toLowerCase().indexOf("http") == 0 || testSuiteDocUri.toLowerCase().indexOf("file") == 0) {
            URL docUrl = new URL(testSuiteDocUri);
            fileName = docUrl.getFile();
        } else
            fileName = new File(testSuiteDocUri).getAbsolutePath();
        return fileName;
    }

    /**
     * The execute method that will be called whenever the ScheduledApp
     * foundation determines that this command should be executed. This execute
     * method will read the TestSuite document associated to the application,
     * execute the Point-To-Point messaging actions specified and verify the
     * results according to the information found in the TestSuite XML document
     * associated to this instance of the application.
     * <P>
     * 
     * @return int just an indicator that the ScheduledApp can use to determine
     *         if the command executed successfully. -1 indicates unsuccessful
     *         execution but all error handling specific to this command is
     *         handled by the command.
     * @throws ScheduleCommandException
     * @see org.openeai.afa.ScheduledApp
     **/
    @Override
    public int execute() throws ScheduledCommandException {

        String errMessage = "";

        boolean useThreads = new Boolean(getProperties().getProperty("useThreads", "false")).booleanValue();
        ThreadPool tPool = null;
        try {
            tPool = (ThreadPool) getAppConfig().getObject("TestSuiteThreadPool");
        } catch (Exception e) {
            // very bad error
        }

        // get the TestSuite Element from the TestSuite document
        Element eTestSuite = m_testSuiteDoc.getRootElement();
        if (eTestSuite == null) {
            errMessage = "Could not find the TestSuite Element in the TestSuite document.  " + "Root element is: "
                    + m_testSuiteDoc.getRootElement().getName();
            logger.fatal(errMessage);
            return -1;
        }

        String testSuiteName = eTestSuite.getAttribute("name").getValue();
        logger.info("About to execute TestSuite: " + testSuiteName);
        logger.info("  Description of TestSuite: " + eTestSuite.getChild("Description").getText());

        m_startTime = new Datetime("RequestReplyStartDatetime");
        java.util.List lTestSeries = eTestSuite.getChildren("TestSeries");
        cnt_totalSeries = lTestSeries.size();
        for (int i = 0; i < lTestSeries.size(); i++) {

            TestId testId = null;
            try {
                testId = (TestId) ((TestId) getAppConfig().getObject("TestId")).clone();
                try {
                    testId.setTestSuiteName(testSuiteName);
                } catch (EnterpriseFieldException e) {
                    errMessage = "Exception setting TestSuiteName on TestId object.  Cannot execute testSuite '" + testSuiteName
                            + "'  Exception: " + e.getMessage();
                    logger.fatal(errMessage);
                    logger.fatal(e.getMessage(), e);
                    return -1;
                }
            } catch (Exception e) {
                // very bad error
            }

            Element eTestSeries = (Element) lTestSeries.get(i);
            if (useThreads) {
                try {
                    tPool.addJob(new ProcessSeries(testId, eTestSeries));
                } catch (Exception e) {
                    // should never happen.
                }
            } else {
                new ProcessSeries(testId, eTestSeries).run();
            }
        }

        logger.info("Waiting for Threads to complete.");
        while (tPool.getJobsInProgress() > 0) {
            try {
                Thread.sleep(500);
            } catch (Exception e) {
            }
        }
        logger.info("All Threads are complete.");

        m_endTime = new Datetime("RequestReplyEndDatetime");

        if (m_failedSteps.size() == 0) {
            logger.info("There were no failed 'RequestReply' tests!");
        } else {
            logger.info("The following 'RequestReply' Tests failed:");
            Iterator it = m_failedSteps.keySet().iterator();
            while (it.hasNext()) {
                String key = (String) it.next();
                errMessage = (String) m_failedSteps.get(key);
                logger.info("FAILURE: " + key + " - " + errMessage);
            }
        }

        if (m_passedSteps.size() == 0) {
            logger.info("There were no 'RequestReply' Tests that passed.");
        } else {
            logger.info("The following 'RequestReply' Tests passed:");
            Iterator it = m_passedSteps.keySet().iterator();
            while (it.hasNext()) {
                String key = (String) it.next();
                logger.info("PASSED: " + key);
            }
        }

        // add the suite summary element to the end of the summary document.
        // need TestSuiteSummary object (and all child objects)
        try {
            TestSuiteSummary testSuiteSummary = (TestSuiteSummary) ((TestSuiteSummary) getAppConfig().getObject("TestSuiteSummary"))
                    .clone();
            testSuiteSummary.setRequestReplyStartDatetime(m_startTime);
            testSuiteSummary.setRequestReplyEndDatetime(m_endTime);

            Iterator it = m_timings.keySet().iterator();
            while (it.hasNext()) {
                logger.info("Setting request timings");
                String key = (String) it.next();
                ArrayList timings = (ArrayList) m_timings.get(key);
                RequestTimings currentTimings = testSuiteSummary.newRequestTimings();
                currentTimings.setAverageTime(getAverageRequestTime(timings) + " milliseconds.");
                currentTimings.setMessageAction(key);
                currentTimings.setNumberOfRequests(Integer.toString(timings.size()));
                MaximumTime max = (MaximumTime) m_maxRequestTimes.get(key);
                if (max != null) {
                    currentTimings.setMaximumTime(max);
                }
                MinimumTime min = (MinimumTime) m_minRequestTimes.get(key);
                if (min != null) {
                    currentTimings.setMinimumTime(min);
                }
                testSuiteSummary.addRequestTimings(currentTimings);
            }

            TestSeriesSummary testSeriesSummary = (TestSeriesSummary) ((TestSeriesSummary) getAppConfig().getObject("TestSeriesSummary"))
                    .clone();
            testSeriesSummary.setTotalSeries(Integer.toString(cnt_totalSeries));
            testSeriesSummary.setPassedSeries(Integer.toString(cnt_passedSeries));
            testSeriesSummary.setFailedSeries(Integer.toString(cnt_failedSeries));
            testSuiteSummary.setTestSeriesSummary(testSeriesSummary);

            TestCaseSummary testCaseSummary = (TestCaseSummary) ((TestCaseSummary) getAppConfig().getObject("TestCaseSummary")).clone();
            testCaseSummary.setTotalCases(Integer.toString(cnt_totalCases));
            testCaseSummary.setPassedCases(Integer.toString(cnt_passedCases));
            testCaseSummary.setFailedCases(Integer.toString(cnt_failedCases));
            testSuiteSummary.setTestCaseSummary(testCaseSummary);

            TestStepSummary testStepSummary = (TestStepSummary) ((TestStepSummary) getAppConfig().getObject("TestStepSummary")).clone();
            testStepSummary.setTotalSteps(Integer.toString(cnt_totalSteps));
            testStepSummary.setPassedSteps(Integer.toString(cnt_passedSteps));
            testStepSummary.setFailedSteps(Integer.toString(cnt_failedSteps));
            testSuiteSummary.setTestStepSummary(testStepSummary);

            Element eTestSuiteSummary = (Element) testSuiteSummary.buildOutputFromObject();
            Element eOutputTestSuite = m_testSuiteSummaryDoc.getRootElement();
            eOutputTestSuite.addContent(eTestSuiteSummary);
        } catch (Exception e) {
            logger.fatal("Exception occurred adding TestSuiteSummary Element.  Exception: " + e.getMessage());
        }

        // dump the content of the TestSuite-Summary document to a file
        // to the screen for now...
        XMLOutputter xmlOut = new XMLOutputter();
        try {
            xmlOut.output(m_testSuiteSummaryDoc, new FileOutputStream(m_testSuiteSummaryDocFileName));
            /*
             * if (emailSummary) { try { MailService mailService = new
             * MailService(); mailService.setToAddr(getProperties().getProperty(
             * "TestSuiteEmailAddress","jackson4@uillinois.edu"));
             * mailService.setFromAddr("testsuiteapplication@openeai.org");
             * mailService.setSubject("TestSuite " + testId.getTestSuiteName() +
             * " Summary");
             * mailService.setMsgText(xmlOut.outputString(m_testSuiteSummaryDoc
             * )); mailService.setMailHost(props.getProperty("mailHost",
             * "newman.aits.uillinois.edu")); mailService.sendMessage(); } catch
             * (Exception exc) { logger.info("Error sending email message - " +
             * exc); } }
             */
        } catch (Exception e) {
            errMessage = "Error writing TestSuite Summary document " + m_testSuiteSummaryDocFileName + ".  Exception: " + e.getMessage();
            logger.fatal(errMessage);
        }

        // now, we need to start the consumer associated to this
        // ScheduledCommand and let it do it's thing
        // appending it's summary information. When the consumer has consumed
        // everything that it should
        // or if it reaches a maximum time allowed configurable setting, it will
        // shut itself and this application down.
        try {
            startReceiving();
        } catch (Exception e) {
            logger.fatal("Error starting receivers.");
            logger.fatal(e.getMessage(), e);
        }

        if (m_expectedNumberOfSyncs > 0) {
            logger.info("[TestSuiteScheduledCommand] Starting PubSub Consumers to "
                    + "consume and verify sync messages that were published during the PointToPoint portion of the test.");
            SyncVerificationCommand.PROCESS_MESSAGES = true;
            try {
                java.util.List pubSubConsumers = getAppConfig().getObjectsLike("PubSubConsumer");
                for (int i = 0; i < pubSubConsumers.size(); i++) {
                    PubSubConsumer pubSubConsumer = (PubSubConsumer) pubSubConsumers.get(i);
                    try {
                        // pubSubConsumer.startMonitor();
                        pubSubConsumer.startConsumer();
                    } catch (Exception e) {
                        errMessage = "Exception occurred starting consumer " + pubSubConsumer.getConsumerName() + "  Exception: "
                                + e.getMessage();
                        logger.fatal(errMessage, e);
                    }
                }
            } catch (Exception e) {
                // very bad error
            }
        } else {
            logger.info("[TestSuiteScheduledCommand] No Sync messages are expected, not starting Consumer(s).");
            TestSuiteScheduledCommand.STAY_ALIVE = false;
        }

        while (TestSuiteScheduledCommand.STAY_ALIVE) {
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
            }
            if (System.currentTimeMillis() - m_processStartTime.getTime() > m_maxProcessTime) {
                if (SyncVerificationCommand.STARTED_VERIFICATION == false) {
                    // only use this timer if SyncVerification never started.
                    logger.info("Maximum allowed process time has elapsed so the application must now be stopped.");
                    TestSuiteScheduledCommand.STAY_ALIVE = false;
                }
            }
        }

        try {
            finishReceiving();
        } catch (Exception e) {
            logger.fatal("Error starting receivers.");
            logger.fatal(e.getMessage(), e);
        }

        logger.info("All sync messages have been consumed or time has expired, now returning from ScheduledCommand.");

        // Reset success and failure maps, so the test suite can be re-run
        // in a subsequent execution of this command.
        resetExecutionStats();

        return 0;
    }

    private long getAverageRequestTime(java.util.List times) {
        long totalTime = 0;
        for (int i = 0; i < times.size(); i++) {
            Long l = (Long) times.get(i);
            totalTime += l.longValue();
        }
        long avgTime = totalTime / times.size();
        return avgTime;
    }

    private void addFailure(TestId testId, String errMessage) {
        String id = testId.toString();
        m_failedSteps.put(id, errMessage);
    }

    private void addSuccess(TestId testId) {
        String id = testId.toString();
        m_passedSteps.put(id, testId);
    }

    private class ProcessSeries implements java.lang.Runnable {
        private Element m_testSeries = null;
        private TestId m_testId = null;

        public ProcessSeries(TestId testId, Element testSeries) {
            m_testSeries = testSeries;
            m_testId = testId;
        }

        @Override
        public void run() {
            String seriesNumber = null;
            String seriesProducer = null;

            try {
                seriesNumber = m_testSeries.getAttribute("number").getValue();
                seriesProducer = m_testSeries.getAttribute("producerName").getValue();
            } catch (Exception e) {
                String errMessage = "Exception retrieving 'TestSeries@number' or "
                        + "'TestSeries@producerName' Attributes.  Make sure TestSuite XML document is "
                        + "valid!  Cannot execute TestSeries '" + seriesNumber + "' for TestSuite " + m_testId.getTestSuiteName()
                        + "  Exception: " + e.getMessage();
                addFailure(m_testId, errMessage);
                logger.fatal(errMessage);
                logger.fatal(e.getMessage(), e);
                return;
            }

            try {
                m_testId.setTestSeriesNumber(seriesNumber);
            } catch (EnterpriseFieldException e) {
                String errMessage = "Exception setting TestSeriesNumber on TestId object.  Cannot execute TestSeries '" + seriesNumber
                        + "' for TestSuite " + m_testId.getTestSuiteName() + "  Exception: " + e.getMessage();
                addFailure(m_testId, errMessage);
                logger.fatal(errMessage);
                logger.fatal(e.getMessage(), e);
                return;
            }
            Element eSummaryTestSeries = getSummaryElement("TestSeries", m_testId);
            java.util.List lTestCases = m_testSeries.getChildren("TestCase");
            cnt_totalCases += lTestCases.size();
            boolean caseErrorOccurred = false;
            ArrayList failedCases = new ArrayList();
            for (int j = 0; j < lTestCases.size(); j++) {
                boolean stepErrorOccurred = false;
                ArrayList failedSteps = new ArrayList();
                Element eTestCase = (Element) lTestCases.get(j);
                String caseNumber = eTestCase.getAttribute("number").getValue();

                // check for preExecutionSleepInterval and
                // postExecutionSleepInterval for the TestCase
                long tcPreExecutionSleepInterval = 0;
                Attribute atcPreSleep = eTestCase.getAttribute("preExecutionSleepInterval");
                if (atcPreSleep != null) {
                    try {
                        tcPreExecutionSleepInterval = Long.parseLong(atcPreSleep.getValue());
                    } catch (Exception e) {
                        logger.fatal("Exception occurred converting the Test Case Pre execution sleep interval " + atcPreSleep.getValue()
                                + " to a number.  Exception is: " + e.getMessage());
                    }
                }

                long tcPostExecutionSleepInterval = 0;
                Attribute atcPostSleep = eTestCase.getAttribute("postExecutionSleepInterval");
                if (atcPostSleep != null) {
                    try {
                        tcPostExecutionSleepInterval = Long.parseLong(atcPostSleep.getValue());
                    } catch (Exception e) {
                        logger.fatal("Exception occurred converting the Test Case Post execution sleep interval " + atcPostSleep.getValue()
                                + " to a number.  Exception is: " + e.getMessage());
                    }
                }

                try {
                    m_testId.setTestCaseNumber(caseNumber);
                } catch (EnterpriseFieldException e) {
                    String errMessage = "Exception setting TestCaseNumber on TestId object.  Cannot execute TestCase '" + caseNumber
                            + "' for TestSuite " + m_testId.getTestSuiteName() + "  TestSeries " + m_testId.getTestSeriesNumber()
                            + "  Exception: " + e.getMessage();
                    addFailure(m_testId, errMessage);
                    logger.fatal(errMessage);
                    logger.fatal(e.getMessage(), e);
                    return;
                }
                Element eSummaryTestCase = getSummaryElement("TestCase", m_testId);

                sleep(m_testId, tcPreExecutionSleepInterval, "before");

                java.util.List lTestSteps = eTestCase.getChildren("TestStep");
                cnt_totalSteps += lTestSteps.size();
                testStepLoop: for (int k = 0; k < lTestSteps.size(); k++) {
                    Element eTestStep = (Element) lTestSteps.get(k);
                    String stepNumber = eTestStep.getAttribute("number").getValue();

                    TestStepExecutor stepExecutor = null;

                    if (eTestStep.getChild("ClassName") == null) {
                        stepExecutor = new DefaultTestStepExecutor();
                        logger.info("Using default TestStepExecutor");
                    } else {
                        logger.info("Using TestStepExecutor: " + eTestStep.getChildText("ClassName"));
                        try {
                            Class executorClass = Class.forName(eTestStep.getChildText("ClassName"));

                            stepExecutor = (TestStepExecutor) executorClass.newInstance();
                        } catch (ClassNotFoundException e) {
                            logger.fatal("Step Executor class not found.", e);
                        } catch (InstantiationException e) {
                            logger.fatal("Step Executor class not instantiated.", e);
                        } catch (IllegalAccessException e) {
                            logger.fatal("Step Executor class not instantiated.", e);
                        }
                    }

                    // check for preExecutionSleepInterval and
                    // postExecutionSleepInterval for the TestStep
                    long tsPreExecutionSleepInterval = 0;
                    Attribute atsPreSleep = eTestStep.getAttribute("preExecutionSleepInterval");
                    if (atsPreSleep != null) {
                        try {
                            tsPreExecutionSleepInterval = Long.parseLong(atsPreSleep.getValue());
                        } catch (Exception e) {
                            logger.fatal("Exception occurred converting the Test Step Pre execution sleep interval "
                                    + atsPreSleep.getValue() + " to a number.  Exception is: " + e.getMessage());
                        }
                    }
                    long tsPostExecutionSleepInterval = 0;
                    Attribute atsPostSleep = eTestStep.getAttribute("postExecutionSleepInterval");
                    if (atsPostSleep != null) {
                        try {
                            tsPostExecutionSleepInterval = Long.parseLong(atsPostSleep.getValue());
                        } catch (Exception e) {
                            logger.fatal("Exception occurred converting the Test Step Post execution sleep interval "
                                    + atsPostSleep.getValue() + " to a number.  Exception is: " + e.getMessage());
                        }
                    }

                    // check for a different producer for this step. a
                    // producerName attribute isn't
                    // associated to the step, just use the 'seriesProducer'
                    String stepProducerName = seriesProducer;
                    Attribute aStepProducer = eTestStep.getAttribute("producerName");
                    if (aStepProducer != null) {
                        stepProducerName = aStepProducer.getValue();
                    }

                    try {
                        m_testId.setTestStepNumber(stepNumber);

                        // check to see if this test id has already been
                        // executed
                        // if it has, we cannot execute this step.
                        // make this default as true so that it is backward
                        // compatible
                        String checkAlreadyExecutedStr = getProperties().getProperty("checkAlreadyExecuted");
                        boolean checkAlreadyExecuted = ("false".equals(checkAlreadyExecutedStr) ? false : true);
                        if (checkAlreadyExecuted) {
                            if (m_failedSteps.containsKey(m_testId.toString()) || m_passedSteps.containsKey(m_testId.toString())) {

                                String errMessage = "TestId '" + m_testId.toString()
                                        + "' has already been executed.  Cannot execute this test step.  "
                                        + "please make sure the TestSuite document " + getProperties().getProperty("TestSuiteDocUri") + " "
                                        + "is filled out correctly!";

                                logger.fatal(errMessage);
                                return;
                            }
                        }

                        sleep(m_testId, tsPreExecutionSleepInterval, "before");
                        // TODO: make executeTestStep (and execute...Request
                        // methods) return a structure
                        // that contains both the execution message and the
                        // elapsed time for the
                        // request associated to the step.

                        // TJ 4/20/2012: save step id so it can be used
                        // to determine when/if a step is getting data from a
                        // previously executed step in the executor
                        String stepId = m_testId.getTestSuiteName() + "-" + m_testId.getTestSeriesNumber() + "-"
                                + m_testId.getTestCaseNumber() + "-" + m_testId.getTestStepNumber();
                        stepIds.add(stepId);

                        StepExecutionInfo sei = stepExecutor.executeTestStep(getAppConfig(), m_commandConfig, stepProducerName, m_testId,
                                eTestStep);
                        handleTimings(m_testId, sei);

                        addSuccess(m_testId);

                        // TJ - 4/20/2012 - add sei.responseMap to
                        // TestSuiteScheduledCommand.responsemMap
                        // logger.info("Adding " + sei.getResponseMap().size() +
                        // " response keys to
                        // TestSuiteScheduledCommand.responseMap.");
                        // responseMap.putAll(sei.getResponseMap());

                        // need to add status element to this teststep in
                        // summary document.
                        Element eSummaryTestStep = getSummaryElement("TestStep", m_testId);
                        TestStatus testStepStatus = null;
                        try {
                            testStepStatus = (TestStatus) ((TestStatus) getAppConfig().getObject("TestStatus")).clone();
                        } catch (Exception e) {
                            // very bad error
                        }

                        if (stepHasSyncs(eTestStep)) {
                            logger.info("TestStep " + m_testId.toString()
                                    + " has Sync messages associated to it.  Setting status to 'pending'");
                            testStepStatus.setValue(TestSuite.PENDING);
                            // set elapsedRequestTime on TestStatus object.
                            testStepStatus.setElapsedRequestTime(Long.toString(sei.getElapsedTime()));
                        } else {
                            if (sei.getExecutionMessage() == null) {
                                testStepStatus.setValue(TestSuite.PASS);
                                // set elapsedRequestTime on TestStatus object.
                                testStepStatus.setElapsedRequestTime(Long.toString(sei.getElapsedTime()));
                            } else {
                                Failure testStepFailure = new Failure();
                                try {
                                    testStepFailure.addReason(sei.getExecutionMessage());
                                    testStepStatus.setFailure(testStepFailure);
                                    testStepStatus.setValue(TestSuite.PASS);
                                    // set elapsedRequestTime on TestStatus
                                    // object.
                                    testStepStatus.setElapsedRequestTime(Long.toString(sei.getElapsedTime()));
                                } catch (Exception e1) {
                                    // very bad error!
                                }
                            }
                        }
                        eSummaryTestStep.addContent((Element) testStepStatus.buildOutputFromObject());

                        cnt_passedSteps++;

                    } catch (Exception e) {
                        try {
                            failedSteps.add(m_testId.clone());
                            failedCases.add(m_testId.clone());
                        } catch (Exception e5) {
                            // very bad error!
                            logger.fatal(e.getMessage(), e);
                        }
                        stepErrorOccurred = true;
                        caseErrorOccurred = true;
                        String errMessage = "Excepton executing TestStep '" + m_testId.toString()
                                + "', continuing to next TestCase.  Exception: " + e.getMessage();
                        logger.fatal(errMessage, e);
                        addFailure(m_testId, e.getMessage());

                        // need to add status element to this teststep in
                        // summary document.
                        Element eSummaryTestStep = getSummaryElement("TestStep", m_testId);
                        TestStatus testStepStatus = null;
                        try {
                            testStepStatus = (TestStatus) ((TestStatus) getAppConfig().getObject("TestStatus")).clone();
                        } catch (Exception e22) {
                            // very bad error
                        }
                        Failure testStepFailure = new Failure();
                        try {
                            testStepFailure.addReason(e.getMessage());
                            testStepStatus.setFailure(testStepFailure);
                            testStepStatus.setValue(TestSuite.FAIL);
                            eSummaryTestStep.addContent((Element) testStepStatus.buildOutputFromObject());
                        } catch (Exception e1) {
                            // very bad error!
                            logger.fatal(e1.getMessage(), e1);
                        }

                        cnt_failedSteps++;
                        sleep(m_testId, tsPostExecutionSleepInterval, "after");
                        break testStepLoop; // continue to next TestCase (maybe
                                            // should continue to next series if
                                            // any case fails).
                    }
                }

                // add testcase status element to summary document
                if (stepErrorOccurred == false) {
                    cnt_passedCases++;

                    // testcase passed, need to add status element to this
                    // testcase element in summary document.
                    try {
                        TestStatus testCaseStatus = (TestStatus) ((TestStatus) getAppConfig().getObject("TestStatus")).clone();
                        if (caseHasSyncs(eTestCase)) {
                            testCaseStatus.setValue(TestSuite.PENDING);
                        } else {
                            testCaseStatus.setValue(TestSuite.PASS);
                        }
                        eSummaryTestCase.addContent((Element) testCaseStatus.buildOutputFromObject());
                    } catch (Exception e1) {
                        // very bad error!
                        logger.fatal(e1.getMessage(), e1);
                    }
                } else {
                    // testcase failed, need to get all failed teststeps for
                    // this case and add status element to this testcase element
                    // in summary document
                    cnt_failedCases++;
                    TestStatus testCaseStatus = null;
                    try {
                        testCaseStatus = (TestStatus) ((TestStatus) getAppConfig().getObject("TestStatus")).clone();
                    } catch (Exception e) {
                        // very bad error
                    }

                    try {
                        Failure testCaseFailure = new Failure();
                        for (int x = 0; x < failedSteps.size(); x++) {
                            TestId tId = (TestId) failedSteps.get(x);
                            // need to add all failed test steps as "reasons"
                            // for failure
                            testCaseFailure.addReason("TestStep: " + tId.getTestStepNumber() + " failed.");
                        }
                        testCaseStatus.setFailure(testCaseFailure);
                        testCaseStatus.setValue(TestSuite.FAIL);
                        eSummaryTestCase.addContent((Element) testCaseStatus.buildOutputFromObject());
                    } catch (Exception e1) {
                        // very bad error!
                        logger.fatal(e1.getMessage(), e1);
                    }
                }
                sleep(m_testId, tcPostExecutionSleepInterval, "after");
            }

            // add testseries status element to summary document
            if (caseErrorOccurred == false) {
                // testseries passed, need to add status element to this
                // testseries element in summary document.
                cnt_passedSeries++;
                try {
                    TestStatus testSeriesStatus = (TestStatus) ((TestStatus) getAppConfig().getObject("TestStatus")).clone();
                    if (seriesHasSyncs(m_testSeries)) {
                        testSeriesStatus.setValue(TestSuite.PENDING);
                    } else {
                        testSeriesStatus.setValue(TestSuite.PASS);
                    }
                    eSummaryTestSeries.addContent((Element) testSeriesStatus.buildOutputFromObject());
                } catch (Exception e1) {
                    // very bad error!
                    logger.fatal(e1.getMessage(), e1);
                }
            } else {
                // testseries failed, need to get all failed testcases for this
                // series and add status element to this testseries element in
                // summary document
                cnt_failedSeries++;
                TestStatus testSeriesStatus = null;
                try {
                    testSeriesStatus = (TestStatus) ((TestStatus) getAppConfig().getObject("TestStatus")).clone();
                } catch (Exception e) {
                    // very bad error
                }

                try {
                    Failure testSeriesFailure = new Failure();
                    for (int z = 0; z < failedCases.size(); z++) {
                        TestId tcaseId = (TestId) failedCases.get(z);
                        // need to add all test cases that failed as "reasons"
                        testSeriesFailure.addReason("TestCase: " + tcaseId.getTestCaseNumber() + " failed.");
                    }
                    testSeriesStatus.setFailure(testSeriesFailure);
                    testSeriesStatus.setValue(TestSuite.FAIL);
                    eSummaryTestSeries.addContent((Element) testSeriesStatus.buildOutputFromObject());
                } catch (Exception e1) {
                    // very bad error!
                    logger.fatal(e1.getMessage(), e1);
                }
            }
        }
    }

    private Element getSummaryElement(String elementType, TestId tId) {
        Element eTestSuite = m_testSuiteSummaryDoc.getRootElement();
        java.util.List lTestSeries = eTestSuite.getChildren("TestSeries");
        for (int i = 0; i < lTestSeries.size(); i++) {
            Element eTestSeries = (Element) lTestSeries.get(i);
            String seriesNumber = eTestSeries.getAttribute("number").getValue();
            if (seriesNumber.equals(tId.getTestSeriesNumber())) {
                if (elementType.equalsIgnoreCase("testseries")) {
                    return eTestSeries;
                } else {
                    java.util.List lTestCases = eTestSeries.getChildren("TestCase");
                    for (int j = 0; j < lTestCases.size(); j++) {
                        Element eTestCase = (Element) lTestCases.get(j);
                        String caseNumber = eTestCase.getAttribute("number").getValue();
                        if (caseNumber.equals(tId.getTestCaseNumber())) {
                            if (elementType.equalsIgnoreCase("testcase")) {
                                return eTestCase;
                            } else {
                                java.util.List lTestSteps = eTestCase.getChildren("TestStep");
                                for (int k = 0; k < lTestSteps.size(); k++) {
                                    Element eTestStep = (Element) lTestSteps.get(k);
                                    String stepNumber = eTestStep.getAttribute("number").getValue();
                                    if (stepNumber.equals(tId.getTestStepNumber())) {
                                        if (elementType.equalsIgnoreCase("teststep")) {
                                            return eTestStep;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    private boolean seriesHasSyncs(Element eTestSeries) {
        java.util.List lTestCases = eTestSeries.getChildren("TestCase");
        for (int j = 0; j < lTestCases.size(); j++) {
            Element eTestCase = (Element) lTestCases.get(j);
            if (caseHasSyncs(eTestCase)) {
                return true;
            }
        }
        return false;
    }

    private boolean caseHasSyncs(Element eTestCase) {
        java.util.List lTestSteps = eTestCase.getChildren("TestStep");
        for (int k = 0; k < lTestSteps.size(); k++) {
            Element eTestStep = (Element) lTestSteps.get(k);
            if (stepHasSyncs(eTestStep)) {
                return true;
            }
        }
        return false;
    }

    private boolean stepHasSyncs(Element eTestStep) {
        Iterator itr = eTestStep.getChildren().iterator();
        while (itr.hasNext()) {
            Element eData = (Element) itr.next();
            java.util.List createSyncs = eData.getChildren(TestSuite.EXPECTED_CREATE_SYNC);
            if (createSyncs != null && createSyncs.size() > 0) {
                return true;
            }
            java.util.List deleteSyncs = eData.getChildren(TestSuite.EXPECTED_DELETE_SYNC);
            if (deleteSyncs != null && deleteSyncs.size() > 0) {
                return true;
            }
            java.util.List updateSyncs = eData.getChildren(TestSuite.EXPECTED_UPDATE_SYNC);
            if (updateSyncs != null && updateSyncs.size() > 0) {
                return true;
            }
        }
        return false;
    }

    private void purgeObject(String deleteAction, ActionableEnterpriseObject queryJeo, XmlEnterpriseObject queryObject,
            PointToPointProducer p2p) throws ScheduledCommandException {
        java.util.List v = null;
        try {
            v = queryJeo.query(queryObject, p2p);
            logger.info("Cleaning up [" + v.size() + "] " + queryJeo.getClass().getName() + " objects...");
        } catch (Exception e) {
            String errMessage = "Exception querying for the " + queryJeo.getClass().getName() + " we need to tear down: " + e.getMessage();
            logger.fatal(errMessage);
            throw new ScheduledCommandException(e.getMessage(), e);
        }

        if (v != null) {
            for (int i = 0; i < v.size(); i++) {
                ActionableEnterpriseObject returnedJeo = (ActionableEnterpriseObject) v.get(i);

                // now delete the object at the destination...
                try {
                    returnedJeo.delete(deleteAction, p2p);
                    logger.info("Tore down the " + returnedJeo.getClass().getName() + " object.");
                } catch (Exception e) {
                    String errMessage = "Exception deleting the " + returnedJeo.getClass().getName() + " we need to tear down: "
                            + e.getMessage();
                    logger.fatal(errMessage);
                    throw new ScheduledCommandException(e.getMessage(), e);
                }
            }
        }
    }

    private int getExpectedNumberOfSyncs() {
        int expectedSyncCount = 0;
        Element eTestSuite = m_testSuiteDoc.getRootElement();

        java.util.List lTestSeries = eTestSuite.getChildren("TestSeries");
        for (int i = 0; i < lTestSeries.size(); i++) {
            Element eTestSeries = (Element) lTestSeries.get(i);
            java.util.List lTestCases = eTestSeries.getChildren("TestCase");
            for (int j = 0; j < lTestCases.size(); j++) {
                Element eTestCase = (Element) lTestCases.get(j);
                java.util.List lTestSteps = eTestCase.getChildren("TestStep");
                for (int k = 0; k < lTestSteps.size(); k++) {
                    Element eTestStep = (Element) lTestSteps.get(k);
                    Element eData = null;

                    Iterator itr = eTestStep.getChildren().iterator();
                    while (itr.hasNext()) {
                        Element eStepMessageData = (Element) itr.next();
                        java.util.List createSyncs = eStepMessageData.getChildren(TestSuite.EXPECTED_CREATE_SYNC);
                        if (createSyncs != null) {
                            expectedSyncCount += createSyncs.size();
                        }
                        java.util.List deleteSyncs = eStepMessageData.getChildren(TestSuite.EXPECTED_DELETE_SYNC);
                        if (deleteSyncs != null) {
                            expectedSyncCount += deleteSyncs.size();
                        }
                        java.util.List updateSyncs = eStepMessageData.getChildren(TestSuite.EXPECTED_UPDATE_SYNC);
                        if (updateSyncs != null) {
                            expectedSyncCount += updateSyncs.size();
                        }
                    }
                }
            }
        }
        return expectedSyncCount;
    }

    private void sleep(TestId testId, long interval, String type) {
        if (interval > 0) {
            logger.info("Test Id: " + testId + " requires a " + interval + " millisecond delay " + type + " execution.");
            try {
                Thread.sleep(interval);
            } catch (Exception e) {
            }
            logger.info("Test Id: " + testId + " can now be executed.");
        }
    }

    /**
     * adds this timing to the vector of timings.
     * <P>
     * 
     * @param testId
     *            the TestId associated to the TestStep being executed. This is
     *            used if this is the maximum time so we can keep track of which
     *            step took the longest.
     * @param action
     *            the action being performed (create, delete, update or query).
     * @param elapsedTime
     *            the elapsed time taken by this particular request.
     **/
    protected void addTime(TestId testId, String action, long elapsedTime) throws ScheduledCommandException {
        try {
            synchronized (m_timings) {
                ArrayList timings = (ArrayList) m_timings.get(action);
                if (timings == null) {
                    timings = new ArrayList();
                    m_timings.put(action, timings);
                }

                timings.add(new Long(elapsedTime));
            }
        } catch (Exception e) {
            logger.fatal(e.getMessage(), e);
            throw new ScheduledCommandException(e.getMessage(), e);
        }
    }

    /**
     * Checks to see if the elapsed time passed in is the maximum for this
     * particular action.
     * <P>
     * 
     * @param testId
     *            the TestId associated to the TestStep being executed. This is
     *            used if this is the maximum time so we can keep track of which
     *            step took the longest.
     * @param action
     *            the action being performed (create, delete, update or query).
     * @param elapsedTime
     *            the elapsed time taken by this particular request.
     **/
    protected void checkMaximumTimes(TestId testId, String action, long elapsedTime) throws ScheduledCommandException {
        try {
            if (m_maxRequestTimes.size() == 0) {
                MaximumTime m = new MaximumTime();
                m.setValue(Long.toString(elapsedTime));
                m.setTestStepId(testId.toString());
                m_maxRequestTimes.put(action, m);
            } else {
                MaximumTime m = (MaximumTime) m_maxRequestTimes.get(action);
                if (m == null) {
                    m = new MaximumTime();
                    m.setValue(Long.toString(elapsedTime));
                    m.setTestStepId(testId.toString());
                    m_maxRequestTimes.put(action, m);
                } else {
                    long oldMax = new Long(m.getValue()).longValue();
                    if (oldMax < elapsedTime) {
                        m.setValue(Long.toString(elapsedTime));
                        m.setTestStepId(testId.toString());
                    }
                }
            }
        } catch (Exception e) {
            logger.fatal(e.getMessage(), e);
            throw new ScheduledCommandException(e.getMessage(), e);
        }
    }

    /**
     * Checks to see if the elapsed time passed in is the maximum for this
     * particular action.
     * <P>
     * 
     * @param testId
     *            the TestId associated to the TestStep being executed. This is
     *            used if this is the maximum time so we can keep track of which
     *            step took the longest.
     * @param action
     *            the action being performed (create, delete, update or query).
     * @param elapsedTime
     *            the elapsed time taken by this particular request.
     **/
    protected void checkMinimumTimes(TestId testId, String action, long elapsedTime) throws ScheduledCommandException {
        try {
            if (m_minRequestTimes.size() == 0) {
                MinimumTime m = new MinimumTime();
                m.setValue(Long.toString(elapsedTime));
                m.setTestStepId(testId.toString());
                m_minRequestTimes.put(action, m);
            } else {
                MinimumTime m = (MinimumTime) m_minRequestTimes.get(action);
                if (m == null) {
                    m = new MinimumTime();
                    m.setValue(Long.toString(elapsedTime));
                    m.setTestStepId(testId.toString());
                    m_minRequestTimes.put(action, m);
                } else {
                    long oldMin = new Long(m.getValue()).longValue();
                    if (elapsedTime < oldMin) {
                        m.setValue(Long.toString(elapsedTime));
                        m.setTestStepId(testId.toString());
                        m_minRequestTimes.put(action, m);
                    }
                }
            }
        } catch (Exception e) {
            logger.fatal(e.getMessage(), e);
            throw new ScheduledCommandException(e.getMessage(), e);
        }
    }

    protected void handleTimings(TestId testId, StepExecutionInfo sei) throws ScheduledCommandException {
        if (sei == null || sei.getCategory() == null) {
            return;
        }

        checkMinimumTimes(testId, sei.getCategory(), sei.getElapsedTime());
        checkMaximumTimes(testId, sei.getCategory(), sei.getElapsedTime());

        addTime(testId, sei.getCategory(), sei.getElapsedTime());
    }

    protected void gatherReceivers() throws CommandException, EnterpriseConfigurationObjectException {

        logger.debug("gathering receivers.");

        java.util.List receivers = getAppConfig().getObjectsLike("");
        for (int i = 0; i < receivers.size(); i++) {
            if (receivers.get(i) instanceof VerificationReceiver) {
                VerificationReceiver receiver = (VerificationReceiver) receivers.get(i);
                logger.debug("got a receiver.  Calling setup:" + receiver.getClass());
                m_receivers.add(receiver);
                try {
                    receiver.setup(getAppConfig());
                } catch (Exception e) {
                    String errMessage = "Exception occurred starting consumer " + receiver.getReceiverName() + "  Exception: "
                            + e.getMessage();
                    logger.fatal(errMessage, e);
                    throw new CommandException(errMessage, e);
                }
            }
        }
    }

    protected void startCleanup() throws CommandException {
        for (int i = 0; i < m_receivers.size(); i++) {
            VerificationReceiver receiver = (VerificationReceiver) m_receivers.get(i);
            // starts up consumer and returns immediatly
            try {
                receiver.startCleanup();
            } catch (Exception e) {
                String errMessage = "Exception occurred starting consumer " + receiver.getReceiverName() + "  Exception: " + e.getMessage();
                logger.fatal(errMessage, e);
                throw new CommandException(errMessage, e);
            }
        }
    }

    protected void finishCleanup() throws CommandException {
        for (int i = 0; i < m_receivers.size(); i++) {
            VerificationReceiver receiver = (VerificationReceiver) m_receivers.get(i);
            // allows receiver to finish cleanup
            try {
                receiver.startCleanup();
            } catch (Exception e) {
                String errMessage = "Exception occurred starting consumer " + receiver.getReceiverName() + "  Exception: " + e.getMessage();
                logger.fatal(errMessage, e);
                throw new CommandException(errMessage, e);
            }
        }
    }

    protected void startReceiving() throws CommandException {
        for (int i = 0; i < m_receivers.size(); i++) {
            VerificationReceiver receiver = (VerificationReceiver) m_receivers.get(i);
            // allows receiver to start receiving
            try {
                receiver.startReceiving();
            } catch (Exception e) {
                String errMessage = "Exception occurred starting consumer " + receiver.getReceiverName() + "  Exception: " + e.getMessage();
                logger.fatal(errMessage, e);
                throw new CommandException(errMessage, e);
            }
        }
    }

    protected void finishReceiving() throws CommandException {
        for (int i = 0; i < m_receivers.size(); i++) {
            VerificationReceiver receiver = (VerificationReceiver) m_receivers.get(i);
            // allows receiver to finish receving msgs
            try {
                receiver.finishReceiving();
            } catch (Exception e) {
                String errMessage = "Exception occurred starting consumer " + receiver.getReceiverName() + "  Exception: " + e.getMessage();
                logger.fatal(errMessage, e);
                throw new CommandException(errMessage, e);
            }
        }
    }

    protected void resetExecutionStats() {
        m_passedSteps = new HashMap();
        m_failedSteps = new HashMap();
    }

}
