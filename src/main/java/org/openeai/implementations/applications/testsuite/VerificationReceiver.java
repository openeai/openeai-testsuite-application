/**********************************************************************
 This file is part of the OpenEAI sample, reference implementation,
 and deployment management suite created by Tod Jackson
 (tod@openeai.org) and Steve Wheat (steve@openeai.org) at
 the University of Illinois Urbana-Champaign.

 Copyright (C) 2002 The OpenEAI Software Foundation

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 For specific licensing details and examples of how this software
 can be used to implement integrations for your enterprise, visit
 http://www.OpenEai.org/licensing.
 */

package org.openeai.implementations.applications.testsuite;


import org.openeai.config.AppConfig;
import org.openeai.jms.consumer.commands.CommandException;


/**
 * This class will be implemented by all message consumers
 * that consume messages for testing purposes.
 **/
public interface VerificationReceiver {


//--------------------- public Methods ---------------------

    /**
     * This method will be used to setup connection.  Note that
     * this setup is in addition to any setup the command may have
     * through the framework.  This AppConfig object is not that of
     * the individual command, but of the TestSuiteScheduledCommand
     *
     * @param config the application config
     */
    void setup(AppConfig config) throws CommandException;


    String getReceiverName();


    /**
     * This method starts the process of cleaning up any messages
     * that have not been consumed before testing begins.
     * This method should start cleanup process, then return immediatly.
     */
    void startCleanup() throws CommandException;


    /**
     * This method should block until the consumer is certain that
     * all messages have been cleaned up.  Note that this can be
     * a configurable amount of time, or whatever the implementation
     * requires.
     */
    void finishCleanup() throws CommandException;


    /**
     * This method starts the process of consuming test messages.
     * This will be called after the outgoing portions of the tests have
     * all been run.  This method should start the consumption process,
     * then return immediately.
     */
    void startReceiving() throws CommandException;


    /**
     * This method should block until the consumer is certain that
     * all messages have been consumed up.  Note that this can be
     * a configurable amount of time, or whatever the implementation
     * requires.
     */
    void finishReceiving() throws CommandException;
}
