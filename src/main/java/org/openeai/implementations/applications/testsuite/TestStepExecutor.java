/**********************************************************************
 This file is part of the OpenEAI sample, reference implementation,
 and deployment management suite created by Tod Jackson
 (tod@openeai.org) and Steve Wheat (steve@openeai.org) at
 the University of Illinois Urbana-Champaign.

 Copyright (C) 2002 The OpenEAI Software Foundation

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 For specific licensing details and examples of how this software
 can be used to implement integrations for your enterprise, visit
 http://www.OpenEai.org/licensing.
 */

package org.openeai.implementations.applications.testsuite;


import org.jdom.Element;
import org.openeai.afa.ScheduledCommandException;
import org.openeai.config.AppConfig;
import org.openeai.config.CommandConfig;
import org.openeai.moa.objects.testsuite.TestId;

/**
 * This interface is implemented by custom test steps
 * to perform the necessary test step activities.
 */
public interface TestStepExecutor {
   /**
     * This method will take a test step and execute it.
     *
     * @param configuration for the scheduled test command
     * @param name          of the producer to use
     * @param test          id for this test case/step
     * @param xml           element from the test document
     * @return a StepExecutionInfo object filled with
     * the results of the test.
     */
    StepExecutionInfo executeTestStep(AppConfig config,
                                             CommandConfig commandConfig, String producerName,
                                             TestId testId, Element eTestStep)
            throws ScheduledCommandException;
}
