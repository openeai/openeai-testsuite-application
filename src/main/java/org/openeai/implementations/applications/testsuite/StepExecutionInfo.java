/**********************************************************************
 This file is part of the OpenEAI sample, reference implementation,
 and deployment management suite created by Tod Jackson
 (tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
 the University of Illinois Urbana-Champaign.

 Copyright (C) 2002 The OpenEAI Software Foundation

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 For specific licensing details and examples of how this software
 can be used to implement integrations for your enterprise, visit
 http://www.OpenEai.org/licensing.
 */

package org.openeai.implementations.applications.testsuite;

import org.openeai.moa.XmlEnterpriseObject;

import java.util.HashMap;

/**
 * This class holds the information about each test step.
 */
public class StepExecutionInfo {

    // --------------------- public Methods ---------------------

    private String executionMessage = null;
    private long elapsedTime = 0;
    private String category = null;
    // TJ 4/20/2012 - added
    private HashMap<String, XmlEnterpriseObject> responseMap = new HashMap<>();


    public void setElapsedTime(long elapsedTime) {
        this.elapsedTime = elapsedTime;
    }

    public void setExecutionMessage(String executionMessage) {
        this.executionMessage = executionMessage;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * This returns the executionMessage information.
     */
    public String getExecutionMessage() {
        return executionMessage;
    }

    /**
     * This returns the category information.
     */
    public String getCategory() {
        return category;
    }

    /**
     * This returns the elapsedTime the step required to compelete.
     */
    public long getElapsedTime() {
        return elapsedTime;
    }

    public HashMap<String, XmlEnterpriseObject> getResponseMap() {
        return responseMap;
    }

    public void setResponseMap(HashMap<String, XmlEnterpriseObject> responseMap) {
        this.responseMap = responseMap;
    }
}
