/**********************************************************************
 This file is part of the OpenEAI sample, reference implementation,
 and deployment management suite created by Tod Jackson
 (tod@openeai.org) and Steve Wheat (steve@openeai.org) at 
 the University of Illinois Urbana-Champaign.

 Copyright (C) 2002 The OpenEAI Software Foundation

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 For specific licensing details and examples of how this software
 can be used to implement integrations for your enterprise, visit
 http://www.OpenEai.org/licensing.
 */

package org.openeai.implementations.applications.testsuite;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.XMLOutputter;
import org.openeai.config.CommandConfig;
import org.openeai.config.LoggerConfig;
import org.openeai.config.PropertyConfig;
import org.openeai.jms.consumer.commands.CommandException;
import org.openeai.jms.consumer.commands.SyncCommand;
import org.openeai.jms.consumer.commands.SyncCommandImpl;
import org.openeai.moa.XmlEnterpriseObject;
import org.openeai.moa.objects.resources.Datetime;
import org.openeai.moa.objects.testsuite.Failure;
import org.openeai.moa.objects.testsuite.TestCaseSummary;
import org.openeai.moa.objects.testsuite.TestId;
import org.openeai.moa.objects.testsuite.TestSeriesSummary;
import org.openeai.moa.objects.testsuite.TestStatus;
import org.openeai.moa.objects.testsuite.TestStepSummary;
import org.openeai.moa.objects.testsuite.TestSuiteSummary;
import org.openeai.xml.XmlDocumentReader;

import javax.jms.Message;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * This is an OpenEAI SyncCommand that consumes sync messages published by an
 * authoritative source when that system creates, updates or deletes an object.
 * It works in conjunction with the TestSuiteScheduledCommand to verify the
 * appropriate sync messages are published. These two commands (this command and
 * the TestSuiteScheduledCommand) make up the OpenEAI "TestSuite" Application.
 * <P>
 * <B>Configuration Parameters:</B>
 * <P>
 * These are the configuration parameters associated to this command.
 * <P>
 * <TABLE BORDER=2 CELLPADDING=5 CELLSPACING=2>
 * <TR>
 * <TH>Property Name</TH>
 * <TH>Required</TH>
 * <TH>Description</TH>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>TestSuiteDocUri</TD>
 * <TD>yes</TD>
 * <TD>The URL for the actual TestSuite associated to this instance of the
 * TeseSuiteApplication. This is the same value used by the
 * TestSuiteScheduledCommand that points to the TestSuite document used by both
 * commands to compare expected with actual results. The TestSuite document
 * contains all the instructions that are to be carried out by the
 * TestSuiteScheduledCommand as well as the expected sync messages that this
 * command expects to consume.</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>TestSuiteSummaryDocPath</TD>
 * <TD>yes</TD>
 * <TD>This is the output path (directory name) where the TestSuite summary
 * document should be written to. Both the TestSuiteScheculedCommand and this
 * command need this parameter. They will both write the results of their
 * processing to a file in this directory. The name of the summary document will
 * be the actual file name of the test suite document with the word "-summary"
 * appdended to it. For example, if the TestSuiteDocUri is:
 * http://xml.openeai.org
 * /xml/configs/messaging/environments/development/tests/1.0
 * /TestSuite_BasicPerson_v1_0.xml then the summary document name would be
 * TestSuite_BasicPerson_v1_0-summary.xml and it would be written to the
 * directory specified in this property (e.g. "./").</TD>
 * </TR>
 * <TR HALIGN="left" VALIGN="top">
 * <TD>maxConsumptionTime</TD>
 * <TD>no (default=30000 or 30 seconds)</TD>
 * <TD>This is the maximum time (specified in milliseconds) that this command
 * will attempt to consume messages after the first message has been consumed
 * AND processed. When the first message is processed, a timer is started and
 * the command does not return. Instead, that instance of the command execution
 * will wait until either the actual number of sync messages consumed equals the
 * expected number of syncs or the 'maxConsumptionTime' has elapsed. If either
 * trigger point is met, the command will return from the execution and the
 * application will stop itself.</TD>
 * </TR>
 * </TABLE>
 * 
 * @author Tod Jackson
 * @see TestSuiteScheduledCommand
 */
public class SyncVerificationCommand extends SyncCommandImpl implements SyncCommand {
    private static Logger logger = Logger.getLogger(SyncVerificationCommand.class);
    private static String LOGTAG = "[" + SyncVerificationCommand.class.getSimpleName() + "]";

    private Document m_testSuiteDoc = null;
    // private HashMap m_failedSteps = new HashMap();
    // private ArrayList m_passedSteps = new ArrayList();
    private int m_expectedNumberOfSyncs = 0;
    private int m_actualNumberOfSyncs = 0;
    private String m_testSuiteSummaryDocPath = "";
    private String m_testSuiteSummaryDocFileName = "";
    private Document m_testSuiteSummaryDoc = null;
    private long m_maxConsumptionTime = 30000; // maximum number of milliseconds
                                               // that we want this consumer to
                                               // stay alive
    private boolean m_firstMessage = false;
    private boolean m_needToInitializeTestSuiteDoc = true;

    private java.util.Date m_startTime = null;
    private Datetime m_startDatetime = null;
    private Datetime m_endTime = null;

    private int cnt_totalSeries = 0;
    private int cnt_passedSeries = 0;
    private int cnt_failedSeries = 0;

    private int cnt_totalCases = 0;
    private int cnt_passedCases = 0;
    private int cnt_failedCases = 0;

    private int cnt_totalSteps = 0;
    private int cnt_passedSteps = 0;
    private int cnt_failedSteps = 0;

    public static boolean PROCESS_MESSAGES = true;
    public static java.util.Date LAST_CLEANUP_TIME = new java.util.Date();
    public static boolean STARTED_VERIFICATION = false;

    /**
     * This is the constructor used by the PubSubConsumer to instantiate this
     * command. The consumer will instantiate the command and then call its
     * 'execute' method when it consumes a message that is supposed to be
     * processed by this command.
     * <P>
     * 
     * @param CommandConfig
     *            the command's configuration information that is specified in
     *            the Deployment document for the application.
     **/
    public SyncVerificationCommand(CommandConfig cConfig) throws InstantiationException {
        super(cConfig);

        try {
            LoggerConfig lConfig = new LoggerConfig();
            lConfig = (LoggerConfig) getAppConfig().getObjectByType(lConfig.getClass().getName());
            PropertyConfigurator.configure(lConfig.getProperties());
        } catch (Exception e) {
        }

        try {
            PropertyConfig pConfig = (PropertyConfig) getAppConfig().getObject("TestSuiteProperties");
            setProperties(pConfig.getProperties());

            m_maxConsumptionTime = Integer.parseInt(getProperties().getProperty("maxConsumptionTime", "30000"));
            XmlDocumentReader xmlReader = new XmlDocumentReader();
            String testSuiteDocUri = getProperties().getProperty("TestSuiteDocUri");

            // make relative path work george wang 2/9/2016
            String fileName = TestSuiteScheduledCommand.docUriToFileName(testSuiteDocUri);
            logger.info("testfileName=" + fileName);
            m_testSuiteDoc = xmlReader.initializeDocument(fileName, getInboundXmlValidation());

            if (m_testSuiteDoc == null) {
                throw new InstantiationException("Missing 'TestSuiteDocUri' property in configuration document.  Can't continue.");
            }

            // URL docUrl = new URL(testSuiteDocUri);
            // String fileName = docUrl.getFile();
            // m_testSuiteSummaryDocPath =
            // getProperties().getProperty("TestSuiteSummaryDocPath", "./");
            // m_testSuiteSummaryDocFileName = m_testSuiteSummaryDocPath
            // + fileName.substring(fileName.lastIndexOf("/") + 1,
            // fileName.indexOf(".xml")) + "-summary.xml";

            m_testSuiteSummaryDocPath = getProperties().getProperty("TestSuiteSummaryDocPath", "./");
            m_testSuiteSummaryDoc = (Document) m_testSuiteDoc.clone();
            m_testSuiteSummaryDocFileName = m_testSuiteSummaryDocPath
                    + fileName.substring(fileName.lastIndexOf("/") + 1, fileName.indexOf(".xml")) + "-summary.xml";
            logger.info("TestSuite Summary file name is: " + m_testSuiteSummaryDocFileName);

            m_expectedNumberOfSyncs = getExpectedNumberOfSyncs();
            logger.info("This command expects there to be " + m_expectedNumberOfSyncs
                    + " sync messages published during the TestSuite execution.");
        } catch (Exception e) {
            logger.fatal("Error initializing 'TestSuiteDocUri' document.");
            logger.fatal(e.getMessage(), e);
            throw new InstantiationException(e.getMessage());
        }

        logger.info("SyncVerificationCommand for gateway " + getAppName() + ", instantiated successfully.");
    }

    private synchronized void incrementActualSyncsConsumed() {
        m_actualNumberOfSyncs++;
        logger.info("Consumed " + m_actualNumberOfSyncs + " messages so far.");
    }

    /**
     * This execute implementation extracts the TestId Element from the
     * ControlArea of the message consumed. Then it proceeds to check the
     * TestSuite document associated to the TestSuiteApplication for expected
     * results. If it finds the TestId in the TestSuite document it will ensure
     * that the message consumed matches the information specified in the
     * TestSuite document as the expected result.
     * <P>
     * If the SyncVerification.PROCESS_MESSAGE variable is false, this command
     * will ignore any messages it consumes. This is controlled by the
     * TestSuiteScheduledCommand. The TestSuiteScheculedCommand must start the
     * consumer associated to this command to ensure the durable subscription is
     * established and to clean off any messages left over from previous runs or
     * from any other sync message publication to this gateway's topic. Then,
     * when it's ready, it will set the SyncVerification.PROCESS_MESSAGE flag to
     * true which will allow this command to actually process the
     * synchronization messages it consumes to verify they are correct according
     * to the test suite document.
     * <P>
     * The first time this execute method gets invoked and the
     * SyncVerificationCommand.PROCESS_MESSAGE flag is 'true', a timer will be
     * started and control will not be returned from this particular execution
     * of the command. The first execution of this command will stay alive until
     * all sync messages have been consumed or until the 'maxConsumptionTime'
     * has been reached. Once that period of time has elapsed, the method will
     * return.
     * <P>
     * 
     * @param int the message number assigned to this execution of the command
     *        by the PubSubConsumer that consumed the message.
     * @param Message
     *            the JMS Message consumed by the PubSubConsumer
     * @throws CommandException
     *             if errors occur extracting information out of the JMS Message
     *             passed in. All other errors are handled by the command.
     * @see org.openeai.jms.consumer.PubSubConsumer
     * @see org.openeai.jms.consumer.commands.SyncCommand
     **/
    public void execute(int messageNumber, Message aMessage) throws CommandException {

        logger.info("Execute called with message number: " + messageNumber);

        if (SyncVerificationCommand.PROCESS_MESSAGES == false) {
            logger.info("not ready to process messages yet.");
            SyncVerificationCommand.LAST_CLEANUP_TIME = new java.util.Date();
            return;
        }

        boolean stayAlive = false;

        if (m_firstMessage == false) {
            SyncVerificationCommand.STARTED_VERIFICATION = true;
            m_firstMessage = true;
            m_startTime = new java.util.Date();
            m_startDatetime = new Datetime("SyncConsumptionStartDatetime");
            stayAlive = true;
        }

        String errMessage = "";

        // Create the input document from the JMS Message passed in.
        Document inDoc = null;
        try {
            inDoc = initializeInput(messageNumber, aMessage);
            logger.debug("Message sent in is: \n" + getMessageBody(inDoc));
        } catch (Exception e) {
            errMessage = "Exception occurred processing input message in SyncCommand.  Exception: " + e.getMessage();
            logger.fatal(errMessage);
            logger.fatal("Message sent in is: \n" + getMessageBody(inDoc));

            /*
             * ArrayList errors = new ArrayList();
             * errors.add(buildError("system", "SYNC-1001",
             * "Exception occurred processing input message in SyncCommand"));
             * publishSyncError(new Element("EmptyControlArea"), errors, e);
             */
            throw new CommandException(e.getMessage(), e);
        }

        // Get information out of the message to determine what mapping needs to
        // be performed.
        Element eControlArea = getControlArea(inDoc.getRootElement());
        String msgCategory = eControlArea.getAttribute(MESSAGE_CATEGORY).getValue();
        String msgObject = eControlArea.getAttribute(MESSAGE_OBJECT).getValue();
        String msgAction = eControlArea.getAttribute(MESSAGE_ACTION).getValue();
        String msgType = eControlArea.getAttribute(MESSAGE_TYPE).getValue();
        String msgRelease = eControlArea.getAttribute(MESSAGE_RELEASE).getValue();

        // this will be the document that has already been created by the
        // TestSuiteScheduledCommand
        try {
            if (m_testSuiteSummaryDoc == null) {
                synchronized (m_testSuiteSummaryDocFileName) {
                    if (m_needToInitializeTestSuiteDoc) {
                        m_needToInitializeTestSuiteDoc = false;
                        XmlDocumentReader xmlReader = new XmlDocumentReader();
                        m_testSuiteSummaryDoc = xmlReader.initializeDocument(m_testSuiteSummaryDocFileName, getOutboundXmlValidation());
                    }
                }
            }
        } catch (Exception e) {
            errMessage = "Error initializing 'TestSuiteSummaryDoc' document.";
            logger.fatal(errMessage, e);
            logAndPublishError("MSG-1002", errMessage, inDoc, eControlArea, e);
            waitToReturn(stayAlive);
            return;
        }

        // get the TestId Element from the ControlArea
        Element eTestId = eControlArea.getChild("Sender").getChild("TestId");
        TestId testId = null;
        if (eTestId != null) {
            try {
                testId = (TestId) ((TestId) getAppConfig().getObject("TestId")).clone();
                try {
                    testId.buildObjectFromInput(eTestId);
                } catch (Exception e) {
                    errMessage = "Could not build the TestId object from the TestId Element in the ControlArea.";
                    logAndPublishError("MSG-1002", errMessage, inDoc, eControlArea, e);
                    waitToReturn(stayAlive);
                    return;
                }
            } catch (Exception e) {
                // error
                errMessage = "Could not find a TestId object in AppConfig.";
                logAndPublishError("MSG-1003", errMessage, inDoc, eControlArea);
                waitToReturn(stayAlive);
                return;
            }
        } else {
            // may or may not be an error
            errMessage = "Could not find a TestId Element in the ControlArea of the " + msgCategory + "." + msgObject + "\\" + msgAction
                    + "-" + msgType + " message sent in.";
            logger.warn(errMessage);
            logger.warn("Message sent in is: \n" + getMessageBody(inDoc));
            waitToReturn(stayAlive);
            return;
        }

        // determine where the message object is in the Document base on
        // messageAction.
        String dataAreaChild = "NewData"; // Default, this will work for Create
                                          // and Update
        if (msgAction.equalsIgnoreCase(DELETE_ACTION)) {
            dataAreaChild = "DeleteData";
        }

        // Get the message object (BasicPerson) element out of the document
        Element eMessageObject = inDoc.getRootElement().getChild("DataArea").getChild(dataAreaChild).getChild(msgObject);

        if (eMessageObject == null) {
            // Error!
            errMessage = "Could not find a " + msgObject + " element at DataArea/" + dataAreaChild + "/" + msgObject + " in the "
                    + msgObject + "-" + msgAction + "-" + msgType + " Document passed in";
            logAndPublishError("MSG-1005", errMessage, inDoc, eControlArea);
            waitToReturn(stayAlive);
            return;
        }

        // if the msgAction is Update, we'll need to get the baseline object out
        // of the document as well.
        Element eBaselineMessageObject = null;
        if (msgAction.equalsIgnoreCase(UPDATE_ACTION)) {
            // we need to create a baseline XEO also so we have to get the
            // baseline element...
            eBaselineMessageObject = inDoc.getRootElement().getChild("DataArea").getChild("BaselineData").getChild(msgObject);

            if (eBaselineMessageObject == null) {
                // Error!
                errMessage = "Could not find a " + msgObject + " + element at DataArea/BaselineData/" + msgObject + " in the " + msgObject
                        + "-" + msgAction + "-" + msgAction + " Document passed in";
                logAndPublishError("MSG-1005", errMessage, inDoc, eControlArea);
                waitToReturn(stayAlive);
                return;
            }
        }

        // if the action was 'Delete' we need to get the delete action from the
        // message
        String deleteAction = null;
        if (msgAction.equalsIgnoreCase(DELETE_ACTION)) {
            deleteAction = inDoc.getRootElement().getChild("DataArea").getChild(dataAreaChild).getChild("DeleteAction")
                    .getAttribute("type").getValue();
        }

        // find the corresponding Sync message for this TestId in the TestSuite
        // document and
        // compare it to the one in the message.
        // get the TestSuite Element from the TestSuite document
        Element eTestSuite = m_testSuiteDoc.getRootElement();
        if (eTestSuite == null) {
            errMessage = "Could not find the TestSuite Element in the TestSuite document.  " + "Root element is: "
                    + m_testSuiteDoc.getRootElement().getName();
            logger.fatal(errMessage);
            logAndPublishError("MSG-1006", errMessage, inDoc, eControlArea);
            waitToReturn(stayAlive);
            return;
        }

        // now, go through and try to find an expected Sync message object that
        // corresponds
        // to the "TestId" contained in the message we consumed. Then, we're
        // going to verify
        // that the actual sync we consumed matches what we expected in the test
        // suite.
        java.util.List lTestSeries = eTestSuite.getChildren("TestSeries");
        for (int i = 0; i < lTestSeries.size(); i++) {
            Element eTestSeries = (Element) lTestSeries.get(i);
            String seriesNumber = eTestSeries.getAttribute("number").getValue();
            if (seriesNumber.equalsIgnoreCase(testId.getTestSeriesNumber())) {
                java.util.List lTestCases = eTestSeries.getChildren("TestCase");
                for (int j = 0; j < lTestCases.size(); j++) {
                    Element eTestCase = (Element) lTestCases.get(j);
                    String caseNumber = eTestCase.getAttribute("number").getValue();
                    if (caseNumber.equalsIgnoreCase(testId.getTestCaseNumber())) {
                        java.util.List lTestSteps = eTestCase.getChildren("TestStep");
                        testStepLoop: for (int k = 0; k < lTestSteps.size(); k++) {
                            Element eTestStep = (Element) lTestSteps.get(k);
                            String stepNumber = eTestStep.getAttribute("number").getValue();
                            if (stepNumber.equalsIgnoreCase(testId.getTestStepNumber())) {
                                logger.info("we've found a matching test step for the " + msgObject + "/" + msgAction
                                        + " message I consumed. (" + seriesNumber + "-" + caseNumber + "-" + stepNumber + ")");

                                // since this sync was precipitated by a
                                // request, we need to get the request
                                // object, then get the expected sync object
                                // that we need to compare against.
                                if (msgAction.equalsIgnoreCase(CREATE_ACTION)) {
                                    Element eTestStepMessage = eTestStep.getChild(TestSuite.CREATE_REQUEST);

                                    if (eTestStepMessage == null) {
                                        logger.debug("Finding create request message type attribute.");

                                        Iterator itr = eTestStep.getChildren().iterator();
                                        while (itr.hasNext() && eTestStepMessage == null) {
                                            Element eTemp = (Element) itr.next();
                                            Attribute aMessageType = eTemp.getAttribute(TestSuite.MESSAGE_TYPE_ATTRIB);

                                            if (aMessageType != null && aMessageType.getValue().equals(TestSuite.CREATE_REQUEST)) {
                                                eTestStepMessage = eTemp;
                                                logger.debug("Found create request message type attribute.");
                                            }
                                        }

                                        if (eTestStepMessage == null) {
                                            // try to see if it may have been a
                                            // generate request that
                                            // caused the create sync to be
                                            // published

                                            eTestStepMessage = eTestStep.getChild(TestSuite.GENERATE_REQUEST);

                                            if (eTestStepMessage == null) {
                                                logger.debug("Finding generate request message type attribute.");

                                                Iterator itr2 = eTestStep.getChildren().iterator();
                                                while (itr2.hasNext() && eTestStepMessage == null) {
                                                    Element eTemp = (Element) itr2.next();
                                                    Attribute aMessageType = eTemp.getAttribute(TestSuite.MESSAGE_TYPE_ATTRIB);

                                                    if (aMessageType != null && aMessageType.getValue().equals(TestSuite.GENERATE_REQUEST)) {
                                                        eTestStepMessage = eTemp;
                                                        logger.debug("Found generate request message type attribute.");
                                                    }
                                                }

                                            }
                                            if (eTestStepMessage == null)
                                                eTestStepMessage = eTestStep.getChild(TestSuite.CREATE_SYNC);
                                            if (eTestStepMessage == null) {
                                                break testStepLoop;
                                            }
                                        }
                                    }

                                    java.util.List eCreateSyncList = eTestStepMessage.getChildren(TestSuite.EXPECTED_CREATE_SYNC);
                                    if (eCreateSyncList.size() == 0) {
                                        // summarize(testId, PASS,
                                        // "No expected Create sync messages");
                                        break testStepLoop;
                                    }

                                    logger.debug("Found " + eCreateSyncList.size() + " expected create syncs.");
                                    String xslUri = null;
                                    java.util.ArrayList eNewDataList = new ArrayList();
                                    for (int m = 0; m < eCreateSyncList.size(); m++) {
                                        Element eCreateSync = (Element) eCreateSyncList.get(m);
                                        Element eNewData = eCreateSync.getChild("NewData");
                                        xslUri = eCreateSync.getAttributeValue(DefaultTestStepExecutor.XSL_URI);
                                        // add new data element to list...
                                        java.util.List lNewDataChildren = eNewData.getChildren(); // should
                                                                                                  // only
                                                                                                  // ever
                                                                                                  // be
                                                                                                  // one!
                                        if (lNewDataChildren.size() != 1) {
                                            // throw exception
                                        }

                                        // we will build a message object from
                                        // this element
                                        // and turn it into an element. That
                                        // way, we're gauranteed a consistent
                                        // order etc. for repeatable objects.
                                        Element eExpectedMessageObject = (Element) lNewDataChildren.get(0);
                                        eNewDataList.add(eExpectedMessageObject);
                                    }

                                    Element eExpectedResult = eTestStepMessage.getChild("TestResult");

                                    try {
                                        String comparisonMessage = compare(testId, msgRelease, msgAction, eExpectedResult, eMessageObject,
                                                eNewDataList, xslUri);
                                        summarize(testId, TestSuite.PASS, comparisonMessage);
                                    } catch (Exception e) {
                                        errMessage = "Exception occurred comparing and summarizing expected vs. actual Create results.  Exception: "
                                                + e.getMessage();
                                        logger.fatal(errMessage, e);
                                        summarize(testId, TestSuite.FAIL, e.getMessage());
                                    }
                                } else if (msgAction.equalsIgnoreCase(DELETE_ACTION)) {
                                    Element eTestStepMessage = eTestStep.getChild(TestSuite.DELETE_REQUEST);

                                    if (eTestStepMessage == null) {
                                        logger.debug("Finding delete request message type attribute.");

                                        Iterator itr = eTestStep.getChildren().iterator();
                                        while (itr.hasNext() && eTestStepMessage == null) {
                                            Element eTemp = (Element) itr.next();
                                            Attribute aMessageType = eTemp.getAttribute(TestSuite.MESSAGE_TYPE_ATTRIB);

                                            if (aMessageType != null && aMessageType.getValue().equals(TestSuite.DELETE_REQUEST)) {
                                                eTestStepMessage = eTemp;
                                                logger.debug("Found delete request message type attribute.");
                                            }
                                        }
                                    }
                                    if (eTestStepMessage == null)
                                        eTestStepMessage = eTestStep.getChild(TestSuite.DELETE_SYNC);
                                    if (eTestStepMessage == null) {
                                        break testStepLoop;
                                    }
                                    
                                    java.util.List eDeleteSyncList = eTestStepMessage.getChildren(TestSuite.EXPECTED_DELETE_SYNC);
                                    if (eDeleteSyncList.size() == 0) {
                                        // summarize(testId, TestSuite.PASS,
                                        // "No expected Delete sync messages");
                                        break testStepLoop;
                                    }
                                    String xslUri = null;

                                    java.util.ArrayList eDeleteDataList = new ArrayList();
                                    for (int m = 0; m < eDeleteSyncList.size(); m++) {
                                        Element eDeleteSync = (Element) eDeleteSyncList.get(m);
                                        Element eDeleteData = eDeleteSync.getChild("DeleteData");
                                        xslUri = eDeleteSync.getAttributeValue(DefaultTestStepExecutor.XSL_URI);

                                        if (eDeleteData.getName().equals(eMessageObject.getName())) {
                                            String expectedDeleteAction = eDeleteData.getChild("DeleteAction").getAttribute("type")
                                                    .getValue();
                                            if (deleteAction.equalsIgnoreCase(expectedDeleteAction) == false) {
                                                errMessage = "FAILED TestStep: " + testId.toString() + " actual delete action '"
                                                        + deleteAction + "' does not match expected delete action '" + expectedDeleteAction
                                                        + "'";
                                                logger.fatal(errMessage);
                                                summarize(testId, TestSuite.FAIL, errMessage);
                                                break testStepLoop;
                                            }
                                        }

                                        Element eExpectedMessageObject = eDeleteData.getChild(msgObject);
                                        if (eExpectedMessageObject != null) {
                                            eDeleteDataList.add(eExpectedMessageObject);
                                        }
                                    }

                                    Element eExpectedResult = eTestStep.getChild(TestSuite.DELETE_REQUEST).getChild("TestResult");
                                    try {
                                        String comparisonMessage = compare(testId, msgRelease, msgAction, eExpectedResult, eMessageObject,
                                                eDeleteDataList, xslUri);
                                        summarize(testId, TestSuite.PASS, comparisonMessage);
                                    } catch (Exception e) {
                                        errMessage = "Exception occurred comparing and summarizing expected vs. actual Delete results.  Exception: "
                                                + e.getMessage();
                                        logger.fatal(errMessage, e);
                                        summarize(testId, TestSuite.FAIL, e.getMessage());
                                    }
                                } else if (msgAction.equalsIgnoreCase(UPDATE_ACTION)) {
                                    logger.debug("Getting test step message");
                                    Element eTestStepMessage = eTestStep.getChild(TestSuite.UPDATE_REQUEST);

                                    if (eTestStepMessage == null) {
                                        logger.debug("Finding update request message type attribute.");

                                        Iterator itr = eTestStep.getChildren().iterator();
                                        while (itr.hasNext() && eTestStepMessage == null) {
                                            Element eTemp = (Element) itr.next();
                                            Attribute aMessageType = eTemp.getAttribute(TestSuite.MESSAGE_TYPE_ATTRIB);

                                            if (aMessageType != null && aMessageType.getValue().equals(TestSuite.UPDATE_REQUEST)) {
                                                eTestStepMessage = eTemp;
                                                logger.debug("Found update request message type attribute.");
                                            }
                                        }
                                    }
                                    if (eTestStepMessage == null)
                                        eTestStepMessage = eTestStep.getChild(TestSuite.UPDATE_SYNC);
                                    if (eTestStepMessage == null) {
                                        break testStepLoop;
                                    }
                                    
                                    java.util.List eUpdateSyncList = eTestStepMessage.getChildren(TestSuite.EXPECTED_UPDATE_SYNC);
                                    if (eUpdateSyncList.size() == 0) {
                                        // summarize(testId, TestSuite.PASS,
                                        // "No expected Update sync messages");
                                        break testStepLoop;
                                    }
                                    String xslUri = null;
                                    java.util.ArrayList eNewDataList = new ArrayList();
                                    java.util.ArrayList eBaselineDataList = new ArrayList();
                                    for (int m = 0; m < eUpdateSyncList.size(); m++) {
                                        Element eUpdateSync = (Element) eUpdateSyncList.get(m);
                                        xslUri = eUpdateSync.getAttributeValue(DefaultTestStepExecutor.XSL_URI);
                                        Element eNewData = eUpdateSync.getChild("NewData");
                                        Element eBaselineData = eUpdateSync.getChild("BaselineData");
                                        // add new data element to list...
                                        java.util.List lNewDataChildren = eNewData.getChildren(); // should
                                                                                                  // only
                                                                                                  // ever
                                                                                                  // be
                                                                                                  // one!
                                        if (lNewDataChildren.size() != 1) {
                                            // throw exception
                                        }

                                        // we will build a message object from
                                        // this element
                                        // and turn it into an element. That
                                        // way, we're gauranteed a consistent
                                        // order etc. for repeatable objects.
                                        Element eExpectedNewDataMessageObject = (Element) lNewDataChildren.get(0);
                                        eNewDataList.add(eExpectedNewDataMessageObject);

                                        // add new data element to list...
                                        java.util.List lBaselineDataChildren = eBaselineData.getChildren(); // should
                                                                                                            // only
                                                                                                            // ever
                                                                                                            // be
                                                                                                            // one!
                                        if (lBaselineDataChildren.size() != 1) {
                                            // throw exception
                                        }

                                        // we will build a message object from
                                        // this element
                                        // and turn it into an element. That
                                        // way, we're gauranteed a consistent
                                        // order etc. for repeatable objects.
                                        Element eExpectedBaselineMessageObject = (Element) lBaselineDataChildren.get(0);
                                        eBaselineDataList.add(eExpectedBaselineMessageObject);
                                    }

                                    Element eExpectedResult = eTestStepMessage.getChild("TestResult");

                                    try {
                                        String comparisonMessage = compare(testId, msgRelease, msgAction, eExpectedResult, eMessageObject,
                                                eNewDataList, xslUri);
                                        summarize(testId, TestSuite.PASS, comparisonMessage);
                                    } catch (Exception e) {
                                        errMessage = "Exception occurred comparing and summarizing expected vs. actual Update 'NewData' results.  Exception: "
                                                + e.getMessage();
                                        logger.fatal(errMessage, e);
                                        summarize(testId, TestSuite.FAIL, e.getMessage());
                                        break testStepLoop;
                                    }

                                    try {
                                        String comparisonMessage = compare(testId, msgRelease, msgAction, eExpectedResult,
                                                eBaselineMessageObject, eBaselineDataList, xslUri);
                                        summarize(testId, TestSuite.PASS, comparisonMessage);
                                    } catch (Exception e) {
                                        logger.fatal(
                                                "Exception occurred comparing and summarizing expected vs. actual Update 'Baseline' results.  Exception: "
                                                        + e.getMessage(), e);
                                        summarize(testId, TestSuite.FAIL, e.getMessage());
                                        break testStepLoop;
                                    }
                                } else {
                                    // error
                                }
                            }
                        }
                    }
                }
            }
        }
        incrementActualSyncsConsumed();
        waitToReturn(stayAlive);
        return;
    }

    private void addFinalSummary() throws CommandException {
        // get the summary document from the file system.
        XmlDocumentReader xmlReader = new XmlDocumentReader();
        Document testSuiteSummaryDoc = null;
        synchronized (m_testSuiteSummaryDocFileName) {
            try {
                testSuiteSummaryDoc = xmlReader.initializeDocument(m_testSuiteSummaryDocFileName, false);
            } catch (Exception e) {
                String errMessage = "Exception parsing the Test Suite Summary document at " + m_testSuiteSummaryDocFileName
                        + ".  Final Summarization can't continue.  Exception: " + e.getMessage();
                logger.fatal(errMessage);
                throw new CommandException(errMessage, e);
            }
        }
        if (m_testSuiteSummaryDoc == null) {
            throw new CommandException("Couldn't parse the Test Suite Summary document at " + m_testSuiteSummaryDocFileName
                    + ".  Can't continue.");
        }

        // add final summary to it.
        Element eTestSuite = testSuiteSummaryDoc.getRootElement();
        java.util.List lTestSeries = eTestSuite.getChildren("TestSeries");

        // first, go through all test cases and set their summary based on child
        // test steps
        cnt_totalSeries += lTestSeries.size();
        for (int i = 0; i < lTestSeries.size(); i++) {

            boolean caseErrorsOccurred = false;
            ArrayList failedCases = new ArrayList();

            Element eTestSeries = (Element) lTestSeries.get(i);
            String seriesNumber = eTestSeries.getAttribute("number").getValue();
            java.util.List lTestCases = eTestSeries.getChildren("TestCase");
            cnt_totalCases += lTestCases.size();
            for (int j = 0; j < lTestCases.size(); j++) {

                boolean stepErrorsOccurred = false;
                ArrayList failedSteps = new ArrayList();

                Element eTestCase = (Element) lTestCases.get(j);
                String caseNumber = eTestCase.getAttribute("number").getValue();
                java.util.List lTestSteps = eTestCase.getChildren("TestStep");
                cnt_totalSteps += lTestSteps.size();
                testStepLoop: for (int k = 0; k < lTestSteps.size(); k++) {
                    Element eTestStep = (Element) lTestSteps.get(k);
                    String stepNumber = eTestStep.getAttribute("number").getValue();
                    logger.info("adding final summarization to: " + seriesNumber + "-" + caseNumber + "-" + stepNumber);

                    Element eTestStepStatus = eTestStep.getChild("TestStatus");
                    if (eTestStepStatus == null) {
                        stepErrorsOccurred = true;
                        failedSteps.add(stepNumber);
                    }

                    TestStatus testStepStatus = null;
                    try {
                        testStepStatus = (TestStatus) ((TestStatus) getAppConfig().getObject("TestStatus")).clone();
                        try {
                            testStepStatus.buildObjectFromInput(eTestStepStatus);
                        } catch (Exception e) {
                            // very bad error!
                        }
                    } catch (Exception e) {
                    }

                    if (testStepStatus.getValue().equalsIgnoreCase(TestSuite.PASS)) {
                        // no op
                        cnt_passedSteps++;
                    } else {
                        cnt_failedSteps++;
                        stepErrorsOccurred = true;
                        failedSteps.add(stepNumber);
                    }
                }

                TestStatus testCaseStatus = null;
                try {
                    testCaseStatus = (TestStatus) ((TestStatus) getAppConfig().getObject("TestStatus")).clone();
                } catch (Exception e) {
                    // very bad error!
                }

                eTestCase.removeChild("TestStatus");
                if (stepErrorsOccurred == false) {
                    try {
                        cnt_passedCases++;
                        testCaseStatus.setValue(TestSuite.PASS);
                        eTestCase.addContent((Element) testCaseStatus.buildOutputFromObject());
                    } catch (Exception e) {
                        // very bad error!
                    }
                } else {
                    // need to add status element to this teststep in summary
                    // document.

                    cnt_failedCases++;
                    caseErrorsOccurred = true;
                    failedCases.add(caseNumber);

                    Failure testCaseFailure = new Failure();
                    try {
                        for (int g = 0; g < failedSteps.size(); g++) {
                            String stepNumber = (String) failedSteps.get(g);
                            testCaseFailure.addReason("TestStep: " + stepNumber + " failed or was not executed.");
                        }
                        testCaseStatus.setFailure(testCaseFailure);
                        testCaseStatus.setValue(TestSuite.FAIL);
                        eTestCase.addContent((Element) testCaseStatus.buildOutputFromObject());
                    } catch (Exception e1) {
                        // very bad error!
                    }
                }
            }

            TestStatus testSeriesStatus = null;
            try {
                testSeriesStatus = (TestStatus) ((TestStatus) getAppConfig().getObject("TestStatus")).clone();
            } catch (Exception e) {
                // very bad error
            }

            eTestSeries.removeChild("TestStatus");
            if (caseErrorsOccurred == false) {
                cnt_passedSeries++;
                try {
                    testSeriesStatus.setValue(TestSuite.PASS);
                    eTestSeries.addContent((Element) testSeriesStatus.buildOutputFromObject());
                } catch (Exception e) {
                    // very bad error!
                }
            } else {
                cnt_failedSeries++;
                Failure testSeriesFailure = new Failure();
                try {
                    for (int g = 0; g < failedCases.size(); g++) {
                        String caseNumber = (String) failedCases.get(g);
                        testSeriesFailure.addReason("TestCase: " + caseNumber + " failed or was not executed.");
                    }
                    testSeriesStatus.setFailure(testSeriesFailure);
                    testSeriesStatus.setValue(TestSuite.FAIL);
                    eTestSeries.addContent((Element) testSeriesStatus.buildOutputFromObject());
                } catch (Exception e1) {
                    // very bad error!
                }
            }

        }

        // add the suite summary element to the end of the summary document.
        // need TestSuiteSummary object (and all child objects)
        Element eRequestReplyTestSuiteSummary = eTestSuite.getChild("TestSuiteSummary");
        try {
            TestSuiteSummary testSuiteSummary = (TestSuiteSummary) ((TestSuiteSummary) getAppConfig().getObject("TestSuiteSummary"))
                    .clone();
            if (eRequestReplyTestSuiteSummary != null) {
                testSuiteSummary.buildObjectFromInput(eRequestReplyTestSuiteSummary);
                eTestSuite.removeContent(eRequestReplyTestSuiteSummary);
            }
            testSuiteSummary.setSyncConsumptionStartDatetime(m_startDatetime);
            testSuiteSummary.setSyncConsumptionEndDatetime(m_endTime);

            TestSeriesSummary testSeriesSummary = (TestSeriesSummary) ((TestSeriesSummary) getAppConfig().getObject("TestSeriesSummary"))
                    .clone();
            testSeriesSummary.setTotalSeries(Integer.toString(cnt_totalSeries));
            testSeriesSummary.setPassedSeries(Integer.toString(cnt_passedSeries));
            testSeriesSummary.setFailedSeries(Integer.toString(cnt_failedSeries));
            testSuiteSummary.setTestSeriesSummary(testSeriesSummary);

            TestCaseSummary testCaseSummary = (TestCaseSummary) ((TestCaseSummary) getAppConfig().getObject("TestCaseSummary")).clone();
            testCaseSummary.setTotalCases(Integer.toString(cnt_totalCases));
            testCaseSummary.setPassedCases(Integer.toString(cnt_passedCases));
            testCaseSummary.setFailedCases(Integer.toString(cnt_failedCases));
            testSuiteSummary.setTestCaseSummary(testCaseSummary);

            TestStepSummary testStepSummary = (TestStepSummary) ((TestStepSummary) getAppConfig().getObject("TestStepSummary")).clone();
            testStepSummary.setTotalSteps(Integer.toString(cnt_totalSteps));
            testStepSummary.setPassedSteps(Integer.toString(cnt_passedSteps));
            testStepSummary.setFailedSteps(Integer.toString(cnt_failedSteps));
            testSuiteSummary.setTestStepSummary(testStepSummary);

            Element eTestSuiteSummary = (Element) testSuiteSummary.buildOutputFromObject();
            eTestSuite.addContent(eTestSuiteSummary);
        } catch (Exception e) {
            logger.fatal("Exception occurred adding TestSuiteSummary Element.  Exception: " + e.getMessage());
            logger.fatal(e.getMessage(), e);
        }

        // write the summary document back to the file system.
        try {
            XMLOutputter xmlOut = new XMLOutputter();
            xmlOut.output(testSuiteSummaryDoc, new FileOutputStream(m_testSuiteSummaryDocFileName));
            /*
             * if (emailSummary) { try { MailService mailService = new
             * MailService(); mailService.setToAddr(getProperties().getProperty(
             * "TestSuiteEmailAddress","jackson4@uillinois.edu"));
             * mailService.setFromAddr("testsuiteapplication@openeai.org");
             * mailService.setSubject("TestSuite " + testId.getTestSuiteName() +
             * " Summary");
             * mailService.setMsgText(xmlOut.outputString(m_testSuiteSummaryDoc
             * )); mailService.setMailHost(props.getProperty("mailHost",
             * "newman.aits.uillinois.edu")); mailService.sendMessage(); } catch
             * (Exception exc) { logger.info("Error sending email message - " +
             * exc); } }
             */
        } catch (Exception e) {
            String errMessage = "Error writing TestSuite Summary document " + m_testSuiteSummaryDocFileName + ".  Exception: "
                    + e.getMessage();
            logger.fatal(errMessage, e);
        }
    }

    private synchronized void summarize(TestId testId, String status, String errorMessage) throws CommandException {
        // get the summary document from the file system.
        XmlDocumentReader xmlReader = new XmlDocumentReader();
        Document testSuiteSummaryDoc = null;
        synchronized (m_testSuiteSummaryDocFileName) {
            try {
                testSuiteSummaryDoc = xmlReader.initializeDocument(m_testSuiteSummaryDocFileName, false);
            } catch (Exception e) {
                String errMessage = "Exception parsing the Test Suite Summary document at " + m_testSuiteSummaryDocFileName
                        + ".  Summarization can't continue.  Exception: " + e.getMessage();
                logger.fatal(errMessage);
                throw new CommandException(errMessage, e);
            }
        }
        if (m_testSuiteSummaryDoc == null) {
            throw new CommandException("Couldn't parse the Test Suite Summary document at " + m_testSuiteSummaryDocFileName
                    + ".  Can't continue.");
        }

        // add this summary to it.
        Element eTestSuite = testSuiteSummaryDoc.getRootElement();
        java.util.List lTestSeries = eTestSuite.getChildren("TestSeries");
        for (int i = 0; i < lTestSeries.size(); i++) {
            Element eTestSeries = (Element) lTestSeries.get(i);
            String seriesNumber = eTestSeries.getAttribute("number").getValue();
            if (seriesNumber.equalsIgnoreCase(testId.getTestSeriesNumber())) {
                java.util.List lTestCases = eTestSeries.getChildren("TestCase");
                for (int j = 0; j < lTestCases.size(); j++) {
                    Element eTestCase = (Element) lTestCases.get(j);
                    String caseNumber = eTestCase.getAttribute("number").getValue();
                    if (caseNumber.equalsIgnoreCase(testId.getTestCaseNumber())) {
                        java.util.List lTestSteps = eTestCase.getChildren("TestStep");
                        for (int k = 0; k < lTestSteps.size(); k++) {
                            Element eTestStep = (Element) lTestSteps.get(k);
                            String stepNumber = eTestStep.getAttribute("number").getValue();
                            if (stepNumber.equalsIgnoreCase(testId.getTestStepNumber())) {
                                logger.info("adding summarization to: " + seriesNumber + "-" + caseNumber + "-" + stepNumber);

                                Element eStatus = eTestStep.getChild("TestStatus");
                                TestStatus testStepStatus = null;
                                try {
                                    testStepStatus = (TestStatus) ((TestStatus) getAppConfig().getObject("TestStatus")).clone();
                                    try {
                                        if (eStatus != null) {
                                            // do this so we save any elapsed
                                            // request timing information
                                            // that might have been entered by
                                            // the Scheduled Command.
                                            testStepStatus.buildObjectFromInput(eStatus);
                                        }
                                        logger.info("[SyncVerificationCommand] TestStep 'elapsedRequestTime' is: "
                                                + testStepStatus.getElapsedRequestTime());
                                        eTestStep.removeChild("TestStatus"); // this
                                                                             // is
                                                                             // the
                                                                             // existing
                                                                             // status
                                                                             // that
                                                                             // might
                                                                             // have
                                                                             // been
                                                                             // established
                                                                             // by
                                                                             // the
                                                                             // scheduled
                                                                             // command
                                    } catch (Exception e) {
                                        // very bad error!
                                    }
                                } catch (Exception e) {
                                    // very bad error
                                }

                                if (status.equalsIgnoreCase(TestSuite.PASS)) {
                                    try {
                                        testStepStatus.setValue(status);
                                        eTestStep.addContent((Element) testStepStatus.buildOutputFromObject());
                                    } catch (Exception e) {
                                        // very bad error!
                                        logger.fatal(
                                                "Exception summarizing successful test id execution '" + errorMessage + "': "
                                                        + e.getMessage(), e);
                                    }
                                } else {
                                    // need to add status element to this
                                    // teststep in summary document.
                                    Failure testStepFailure = new Failure();
                                    try {
                                        testStepFailure.addReason(errorMessage);
                                        testStepStatus.setFailure(testStepFailure);
                                        testStepStatus.setValue(TestSuite.FAIL);
                                        eTestStep.addContent((Element) testStepStatus.buildOutputFromObject());
                                    } catch (Exception e2) {
                                        // very bad error!
                                        logger.fatal(
                                                "Exception summarizing failed test id execution '" + errorMessage + "': " + e2.getMessage(),
                                                e2);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        // write the summary document back to the file system.
        try {
            XMLOutputter xmlOut = new XMLOutputter();
            xmlOut.output(testSuiteSummaryDoc, new FileOutputStream(m_testSuiteSummaryDocFileName));
        } catch (Exception e) {
            String errMessage = "Error writing TestSuite Summary document " + m_testSuiteSummaryDocFileName + ".  Exception: "
                    + e.getMessage();
            logger.fatal(errMessage, e);
        }
    }

    private String compare(TestId testId, String msgRelease, String msgAction, Element expectedResult, Element eActualMessageObject,
            java.util.List eExpectedMessageObjectList) throws CommandException {
        return compare(testId, msgRelease, msgAction, expectedResult, eActualMessageObject, eExpectedMessageObjectList, null);

    }

    private String compare(TestId testId, String msgRelease, String msgAction, Element expectedResult, Element eActualMessageObject,
            java.util.List eExpectedMessageObjectList, String xslUri) throws CommandException {

        String comparisonMessage = null;
        XMLOutputter xmlOut = new XMLOutputter();

        try {
            eActualMessageObject = DefaultTestStepExecutor.applyTransformation(xslUri, eActualMessageObject);
        } catch (Exception e) {
            logger.error(e);
            throw new CommandException(e);
        }
        String actualData = xmlOut.outputString(eActualMessageObject);
        String expectedData = "";
        boolean performedComparison = false;
        logger.info("[compare] Actual sync message object name is: " + eActualMessageObject.getName());
        for (int i = 0; i < eExpectedMessageObjectList.size(); i++) {
            Element eExpectedMessageObject = (Element) eExpectedMessageObjectList.get(i);
            logger.info("[compare] Expected sync message object [" + i + "] is: " + eExpectedMessageObject.getName());
            if (eActualMessageObject.getName().equals(eExpectedMessageObject.getName())) {
                performedComparison = true;
                try {
                    XmlEnterpriseObject expectedXeo = (XmlEnterpriseObject) getAppConfig().getObject(
                            eExpectedMessageObject.getName() + "." + generateRelease(msgRelease));

                    // TJ 4/20/2012: apply any data that's been saved for this
                    // step/object
                    // from the shared TestSuiteScheduledCommand.responseMap to
                    // the element that will be used to populate the expectedXeo
                    DefaultTestStepExecutor.applyDataToElementFromResponseMap(eExpectedMessageObject);

                    expectedXeo.buildObjectFromInput(eExpectedMessageObject);
                    expectedData = xmlOut.outputString((Element) expectedXeo.buildOutputFromObject());
                } catch (Exception e) {
                    // very bad thing!
                    throw new CommandException(e.getMessage(), e);
                }

                // check that the actual message action equals the expected
                // action
                String expectedAction = expectedResult.getAttribute("action").getValue();

                // if the expected action is 'generate' we need to change it to
                // 'Create'
                // because a generate request will result in a Create-Sync.
                // however, the generate action is also used in the
                // point-to-point verification
                // so we really don't have a good way to detect it now other
                // than here...
                // TODO: add GENERATE_ACTION to ConsumerCommand.
                if (expectedAction.equalsIgnoreCase(TestSuiteScheduledCommand.GENERATE_ACTION)) {
                    expectedAction = TestSuiteScheduledCommand.CREATE_ACTION;
                }
                if (msgAction.equals(expectedAction) == false) {
                    String errMessage = "FAILED TestStep: 'messageAction' (" + msgAction
                            + ") in message consumed doesn't match expected messageAction (" + expectedAction + ")";
                    throw new CommandException(errMessage);
                }

                String expectedStatus = expectedResult.getAttribute("status").getValue();
                if (actualData.equals(expectedData) == false) {
                    // error

                    // if the expected status is FAIL then this is not a failure
                    // but we need to return the fact that the failure was
                    // expected and why
                    // it failed.
                    if (expectedStatus.equalsIgnoreCase(TestSuite.SUCCESS)) {
                        StringBuffer sBuf = new StringBuffer();
                        sBuf.append("FAILED TestStep: " + testId.toString() + "\n");
                        sBuf.append("Actual data in Sync message consumed: \n" + actualData + "\n");
                        sBuf.append("Expected data in TestSuite document: \n" + expectedData + "\n");
                        throw new CommandException(sBuf.toString());
                    } else {
                        StringBuffer sBuf = new StringBuffer();
                        sBuf.append("PASSED TestStep with expected actual vs. expected mismatch: " + testId.toString() + "\n");
                        sBuf.append("Actual data in Sync message consumed: \n" + actualData + "\n");
                        sBuf.append("Expected data in TestSuite document: \n" + expectedData + "\n");
                        comparisonMessage = sBuf.toString();
                    }
                } else {
                    // it passed.
                    logger.info("PASSED TestStep: " + testId.toString());
                }
            }
        }

        if (performedComparison == false) {
            String errMessage = "FAILED TestStep: actual message object '" + eActualMessageObject.getName()
                    + "' contained in sync could not be found in the list of expected message objects.";
            logger.fatal(errMessage);
            throw new CommandException(errMessage);
        }

        return comparisonMessage;
    }

    private void waitToReturn(boolean stayAlive) {
        if (stayAlive) {
            logger.info("I'm the command that's suppose to stay alive and wait for "
                    + "either all messages have been consumed or the maximum allowed period of "
                    + "time has elapsed.  So, I'm not returning.");
        } else {
            return;
        }
        while (stayAlive) {
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
            }
            if (m_actualNumberOfSyncs >= m_expectedNumberOfSyncs) {
                logger.info("We have successfully processed all syncs that were expected.  All Consumption is complete.  The application can now be stopped.");
                stayAlive = false;
            }
            if (System.currentTimeMillis() - m_startTime.getTime() > m_maxConsumptionTime) {
                logger.info("Haven't consumed all messages I'm suppose to but maximum allowed time has elapsed so the application must now be stopped.");
                stayAlive = false;
            }
        }

        // go through all testseries, testcases and teststeps in the summary
        // document
        // and add the final summary information
        try {
            m_endTime = new Datetime("SyncConsumptionEndDatetime");
            addFinalSummary();
        } catch (Exception e) {
            // very bad error!
        }

        logger.info("Signalling the TestSuiteScheduledCommand to exit.");
        TestSuiteScheduledCommand.STAY_ALIVE = false;
        return;
    }

    private int getExpectedNumberOfSyncs() {
        int expectedSyncCount = 0;
        Element eTestSuite = m_testSuiteDoc.getRootElement();

        java.util.List lTestSeries = eTestSuite.getChildren("TestSeries");
        for (int i = 0; i < lTestSeries.size(); i++) {
            Element eTestSeries = (Element) lTestSeries.get(i);
            java.util.List lTestCases = eTestSeries.getChildren("TestCase");
            for (int j = 0; j < lTestCases.size(); j++) {
                Element eTestCase = (Element) lTestCases.get(j);
                java.util.List lTestSteps = eTestCase.getChildren("TestStep");
                for (int k = 0; k < lTestSteps.size(); k++) {
                    Element eTestStep = (Element) lTestSteps.get(k);
                    Element eData = null;
                    Iterator itr = eTestStep.getChildren().iterator();
                    while (itr.hasNext()) {
                        Element eStepMessageData = (Element) itr.next();
                        java.util.List createSyncs = eStepMessageData.getChildren(TestSuite.EXPECTED_CREATE_SYNC);
                        if (createSyncs != null) {
                            expectedSyncCount += createSyncs.size();
                        }
                        java.util.List deleteSyncs = eStepMessageData.getChildren(TestSuite.EXPECTED_DELETE_SYNC);
                        if (deleteSyncs != null) {
                            expectedSyncCount += deleteSyncs.size();
                        }
                        java.util.List updateSyncs = eStepMessageData.getChildren(TestSuite.EXPECTED_UPDATE_SYNC);
                        if (updateSyncs != null) {
                            expectedSyncCount += updateSyncs.size();
                        }
                    }
                }
            }
        }
        return expectedSyncCount;
    }

    private void logAndPublishError(String errNumber, String errMessage, Document inDoc, Element eControlArea, Exception e) {
        logger.fatal(errMessage);
        logger.fatal("Message sent in is: \n" + getMessageBody(inDoc));
    }

    private void logAndPublishError(String errNumber, String errMessage, Document inDoc, Element eControlArea) {
        logger.fatal(errMessage);
        logger.fatal("Message sent in is: \n" + getMessageBody(inDoc));
    }
}