package org.openeai.implementations.applications.testsuite;

public abstract class TestSuite {
    protected final static String SUCCESS = "success";
    protected final static String PASS = "pass";
    protected final static String FAIL = "fail";
    protected final static String PENDING = "pending";
    protected final static String EXPECTED_CREATE_SYNC = "ExpectedCreateSync";
    protected final static String EXPECTED_DELETE_SYNC = "ExpectedDeleteSync";
    protected final static String EXPECTED_UPDATE_SYNC = "ExpectedUpdateSync";
    protected final static String QUERY_REQUEST = "QueryRequest";
    protected final static String GENERATE_REQUEST = "GenerateRequest";
    protected final static String CREATE_REQUEST = "CreateRequest";
    protected final static String DELETE_REQUEST = "DeleteRequest";
    protected final static String UPDATE_REQUEST = "UpdateRequest";
    protected final static String UPDATE_SYNC = "UpdateSync";
    protected final static String CREATE_SYNC = "CreateSync";
    protected final static String DELETE_SYNC = "DeleteSync";
    protected final static String MESSAGE_TYPE_ATTRIB = "messageType";
}